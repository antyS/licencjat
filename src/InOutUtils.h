#ifndef LICENCJAT_INOUTUTILS_H
#define LICENCJAT_INOUTUTILS_H

#include "common/libs/json.hpp"

using namespace std;
using json = nlohmann::json;

class InOutUtils {
public:
    static void saveJson(const json &toSave, string &filename);

    static void openFile(fstream &file, string &filename, ios_base::openmode __mode);

    static json readJson(string &filename);

    static bool is_file_exist(const string &fileName);

    static string pathToJson(const string &filename);

private:

    static bool has_suffix(const string &str, const string &suffix);
};

#endif //LICENCJAT_INOUTUTILS_H

#include <tuple>
#include "CompactTrie.h"

uint32_t CompactTrie::search(const string &key) {
    auto orLast = searchOrLast(key);
    if (isPresent(key, orLast)) {
        return get<0>(orLast)->val;
    }
    return NULL;
}

/**
 *
 * @param key
 * @return 0 - last accessed node
 * @return 1 - number of matched chars from key
 * @return 2 - number of matched chars from key in edgePrefix
 */
tuple<CompactTrieNode *, unsigned int, unsigned int> CompactTrie::searchOrLast(const string &key) const {
    auto *actual = (CompactTrieNode *) root, *parent = (CompactTrieNode *) root;
    uint32_t i;
    for (i = 0; actual != nullptr && i < key.size(); i++) {
        parent = actual;
        if (!actual->edgePrefix.empty()) {
            uint32_t pLen = prefixLen(actual->edgePrefix, key.substr(i));
            i += pLen;
            if (pLen < actual->edgePrefix.size() || i >= key.size()) {
                return make_tuple(actual, i, pLen);
            }
        }
        actual = actual->children[key[i]];
    }
    if (actual != nullptr) {
        return make_tuple(actual, i, 0);
    } else {
        return make_tuple(parent, i - 1, parent->edgePrefix.size());
    }
}

bool CompactTrie::insert(const string &key, const uint32_t &value) {
    auto orLast = searchOrLast(key);
    if (isPresent(key, orLast)) {
        return false;
    }
    CompactTrieNode *actual = get<0>(orLast);
    auto matchedEdgeCnt = get<2>(orLast);
    auto matchedKeyCnt = get<1>(orLast);
    if (this->size <= 0) {// when  root
        actual->edgePrefix = key;
        actual->val = value;
        size++;
        return true;
    }
    if (matchedEdgeCnt < actual->edgePrefix.length()) {//split
        auto created = new CompactTrieNode(actual->edgePrefix.substr(0, matchedEdgeCnt));
        created->parent = actual->parent;
        if (actual->parent != nullptr) {
            actual->parent->children[key[matchedKeyCnt - matchedEdgeCnt - 1]] = created;
        } else {
            root = created;
        }
        created->children[actual->edgePrefix[matchedEdgeCnt]] = actual;
        created->childCount++;
        actual->parent = created;
        actual->edgePrefix = actual->edgePrefix.substr(matchedEdgeCnt + 1);
        actual = created;
    }
    if (matchedKeyCnt < key.size()) {
        auto created = new CompactTrieNode(key.substr(matchedKeyCnt + 1));
        created->parent = actual;
        actual->children[key[matchedKeyCnt]] = created;
        actual->childCount++;
        actual = created;
    }
    actual->val = value;
    size++;
    return true;
}

bool CompactTrie::remove(const string &key) {
    auto orLast = searchOrLast(key);
    if (!isPresent(key, orLast)) {
        return false;
    }
    CompactTrieNode *actual = get<0>(orLast);
    actual->val = NULL;
    auto parent = actual->parent;
    auto matchedPrefixCnt = get<2>(orLast);
    if (actual->childCount <= 1) {
        int i = actual->firstChildIndex();
        if (i != -1) {//one child
            auto child = actual->children[i];
            actual->children[i] = nullptr;
            child->edgePrefix = actual->edgePrefix + (char) i + child->edgePrefix;
            child->parent = parent;
            if (parent != nullptr) {
                parent->children[key[key.size() - 1 - matchedPrefixCnt]] = child;
            } else {
                root = child;
            }
            delete actual;

        } else {//no child
            if (parent != nullptr) {
                parent->children[key[key.size() - 1 - matchedPrefixCnt]] = nullptr;
                parent->childCount--;
                delete actual;
            } else {
                actual->edgePrefix = "";//delete when in root
            }
        }

    }
    if (parent != nullptr && parent->childCount <= 1 && parent->val == NULL) {//compress parent
        int i = parent->firstChildIndex();
        if (i != -1) {
            auto child = parent->children[i];
            parent->children[i] = nullptr;
            child->edgePrefix = parent->edgePrefix + (char) i + child->edgePrefix;
            child->parent = parent->parent;
            if (parent->parent != nullptr) {
                parent->parent->children[key[key.size() - 1
                                             - matchedPrefixCnt -1
                                             - parent->edgePrefix.size()]] = child;
            } else {
                root = child;
            }
            delete parent;

        }
    }
    size--;
    return true;
}

CompactTrie::CompactTrie() {
    this->root = new CompactTrieNode("");
}

bool CompactTrie::isPresent(const string &key, tuple<CompactTrieNode *, unsigned int, unsigned int> orLast) const {
    auto lastNode = get<0>(orLast);
    auto matchedKeyCnt = get<1>(orLast);
    auto matchedEdgeCnt = get<2>(orLast);
    return matchedEdgeCnt == lastNode->edgePrefix.length() && matchedKeyCnt == key.size() && lastNode->val != NULL;
}

uint32_t CompactTrie::prefixLen(const string &prefix, const string &key) const {
    uint32_t i = 0;
    for (; i < prefix.length() && i < key.length(); ++i) {
        if (prefix[i] != key[i]) {
            return i;
        }
    }
    return i;
}


#ifndef LICENCJAT_COMPACTTRIE_H
#define LICENCJAT_COMPACTTRIE_H


#include <cstring>
#include "../Dict.h"
#include "CompactTrieNode.h"


class CompactTrie : public Dict {

public:
    explicit CompactTrie();

    uint32_t search(const string &key) override;

    bool insert(const string &key, const uint32_t &value) override;

    bool remove(const string &key) override;

private:

    uint32_t prefixLen(const string &prefix, const string &key) const;
    tuple<CompactTrieNode *, unsigned int, unsigned int> searchOrLast(const string &key) const;

    bool isPresent(const string &key, tuple<CompactTrieNode *, unsigned int, unsigned int> orLast) const;
};


#endif //LICENCJAT_COMPACTTRIE_H

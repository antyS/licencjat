#include <utility>

#include "CompactTrieNode.h"


CompactTrieNode::~CompactTrieNode() {
    for (auto child:children) {
        delete child;
    }
}

CompactTrieNode::CompactTrieNode(string edgePrefix) {
    this->edgePrefix = std::move(edgePrefix);
}

int CompactTrieNode::firstChildIndex() {
    for (int i = 0; i < CHILDREN_SIZE; ++i) {
        if (this->children[i] != nullptr)
            return i;
    }
    return -1;
}

#ifndef LICENCJAT_COMPACTTRIENODE_H
#define LICENCJAT_COMPACTTRIENODE_H


#include "../Node.h"
#include "../../Consts.h"

static const int CHILDREN_SIZE = MAX_CHAR + 1;

class CompactTrieNode : public Node {
public:
    explicit CompactTrieNode(string edgePrefix);

    CompactTrieNode *parent = nullptr;
    uint32_t val = NULL;
    uint32_t childCount = 0;

    CompactTrieNode *children[CHILDREN_SIZE] = {nullptr};
    string edgePrefix;

    virtual ~CompactTrieNode();

/*
 * @return -1 when no children
 */
    int firstChildIndex();

};


#endif //LICENCJAT_COMPACTTRIENODE_H

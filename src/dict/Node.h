#ifndef LICENCJAT_NODE_H
#define LICENCJAT_NODE_H

#include <string>
#include <ostream>
#include <stdint.h>

using namespace std;

class Node {
public:
    virtual string toString (){ return "Not implemented";};
    virtual ~Node() {
    }

};

#endif //LICENCJAT_NODE_H

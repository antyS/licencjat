#include <tuple>
#include "RedBlack.h"


bool RedBlack::insert(const string &key, const uint32_t &value) {
    pair<BstNode *, bool> result = Bst::inserted(new RBNode(key, value));
    if (result.second) {
        fixInserted((RBNode *) result.first);
    }
    return result.second;
}

void RedBlack::fixInserted(RBNode *node) {
    while (1) {
        RBNode *gParent = grandParent(node);
        RBNode *unc = nullptr;
        if (gParent != nullptr) {
            if (node->parent == gParent->left)
                unc = (RBNode *) gParent->right;
            else if (node->parent == gParent->right)
                unc = (RBNode *) gParent->left;
        }
        if (node == root)
            node->isRed = false;
        else if (isBlack(node->parent)) {
            break;
        } else if (isRed(unc)) {
            ((RBNode *) node->parent)->isRed = false;
            unc->isRed = false;
            gParent->isRed = true;
            node = gParent;
            continue;
        } else {
            if ((node == node->parent->right) && (node->parent == gParent->left)) {
                rotateLeft(node->parent);
                node = (RBNode *) node->left;
            } else if ((node == node->parent->left) && (node->parent == gParent->right)) {
                rotateRight(node->parent);
                node = (RBNode *) node->right;
            }
            gParent = grandParent(node);
            ((RBNode *) (node->parent))->isRed = false;
            gParent->isRed = true;
            if (node == node->parent->left)
                rotateRight(gParent);
            else
                rotateLeft(gParent);
        }
        break;
    }
}

RBNode *RedBlack::grandParent(BstNode *node) {
    if ((node != nullptr) && (node->parent != nullptr))
        return (RBNode *) node->parent->parent;
    return nullptr;
}

bool RedBlack::remove(const string &key) {
    BstNode *toRemove = searchOrLast(key);
    if (toRemove == nullptr || toRemove->key != key) {
        return false;
    }
    BstNode *reallyRemove = nullptr;
    BstNode *child = nullptr;
    bool wasRightRemoved = false;
    if ((toRemove->left == nullptr) || (toRemove->right == nullptr)) {
        reallyRemove = toRemove;
    } else {
        reallyRemove = minKey(toRemove->right);
    }
    if (reallyRemove->left != nullptr) {
        child = reallyRemove->left;
    } else {
        child = reallyRemove->right;
    }
    if (child != nullptr) {
        child->parent = reallyRemove->parent;
    }
    if (reallyRemove->parent == nullptr) {
        root = child;
    } else if (reallyRemove == reallyRemove->parent->left) {
        reallyRemove->parent->left = child;
    } else {
        reallyRemove->parent->right = child;
        wasRightRemoved = true;
    }
    if (reallyRemove != toRemove) {
        reallyRemove->copyValues(toRemove);
    }
    if (isBlack(reallyRemove) && root != nullptr) {
        fixRemove((RBNode *) reallyRemove->parent, (RBNode *) child, wasRightRemoved);
    }
    reallyRemove->left = nullptr;
    reallyRemove->right = nullptr;
    delete reallyRemove;
    size--;
    return true;
}


void RedBlack::fixRemove(RBNode *reallyRemoveParent, RBNode *child, bool wasRightRemoved) {
    RBNode *brother;
    while (isBlack(child) && child != root) {
        if (wasRightRemoved) {
            brother = (RBNode *) reallyRemoveParent->left;
            if (isRed(brother)) {
                brother->isRed = false;
                reallyRemoveParent->isRed = true;
                rotateRight(reallyRemoveParent);
                brother = (RBNode *) reallyRemoveParent->left;
            }
            if (isBlack(brother->right) && isBlack(brother->left)) {
                brother->isRed = true;
                child = reallyRemoveParent;
                reallyRemoveParent = (RBNode *) child->parent;
                wasRightRemoved = (reallyRemoveParent != nullptr && child == reallyRemoveParent->right);
            } else {
                if (isBlack(brother->left)) {
                    ((RBNode *) brother->right)->isRed = false;
                    brother->isRed = true;
                    rotateLeft(brother);
                    brother = (RBNode *) reallyRemoveParent->left;
                }

                brother->isRed = reallyRemoveParent->isRed;
                reallyRemoveParent->isRed = false;
                if (brother->left != nullptr) {
                    ((RBNode *) brother->left)->isRed = false;
                }
                rotateRight(reallyRemoveParent);
                child = (RBNode *) root;
            }
        } else {
            brother = (RBNode *) reallyRemoveParent->right;
            if (isRed(brother)) {
                brother->isRed = false;
                reallyRemoveParent->isRed = true;
                rotateLeft(reallyRemoveParent);
                brother = (RBNode *) reallyRemoveParent->right;
            }

            if (isBlack(brother->left) && isBlack(brother->right)) {
                brother->isRed = true;
                child = reallyRemoveParent;
                reallyRemoveParent = (RBNode *) child->parent;
                wasRightRemoved = (reallyRemoveParent != nullptr && child == reallyRemoveParent->right);
            } else {
                if (isBlack(brother->right)) {
                    ((RBNode *) brother->left)->isRed = false;
                    brother->isRed = true;
                    rotateRight(brother);
                    brother = (RBNode *) reallyRemoveParent->right;
                }

                brother->isRed = reallyRemoveParent->isRed;
                reallyRemoveParent->isRed = false;
                if (brother->right != nullptr) {
                    ((RBNode *) brother->right)->isRed = false;
                }
                rotateLeft(reallyRemoveParent);
                child = (RBNode *) root;
            }
        }
    }
    if (child != nullptr)
        child->isRed = false;
}


bool RedBlack::isRed(BstNode *node) {
    if (node == nullptr) {
        return false;
    }
    return ((RBNode *) node)->isRed;
}

bool RedBlack::isBlack(BstNode *node) {
    if (node == nullptr) {
        return true;
    }
    return !((RBNode *) node)->isRed;
}

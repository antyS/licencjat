#ifndef LICENCJAT_REDBLACK_H
#define LICENCJAT_REDBLACK_H


#include "../Bst/Bst.h"
#include "RBNode.h"

class RedBlack : public Bst {
public:
    bool insert(const string &key, const uint32_t &value) override;

    bool remove(const string &key) override;
private:

    RBNode *grandParent(BstNode *node);

    void fixInserted(RBNode *node);


    bool isRed(BstNode *node);

    bool isBlack(BstNode *node);

    void fixRemove(RBNode *parent, RBNode *child, bool wasRightRemoved);


};


#endif //LICENCJAT_REDBLACK_H

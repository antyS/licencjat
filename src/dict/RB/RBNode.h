#ifndef LICENCJAT_RBNODE_H
#define LICENCJAT_RBNODE_H


#include "../Bst/BstNode.h"

class RBNode : public BstNode {
public:
    RBNode(const string &key, const uint32_t &val);

    bool isRed;

    void printNodeValue(ostream &os) const override;
};


#endif //LICENCJAT_RBNODE_H

#include "RBNode.h"


RBNode::RBNode(const string &key, const uint32_t &val) : BstNode(key, val) {
    isRed = true;
}

void RBNode::printNodeValue(std::ostream &os) const {
    BstNode::printNodeValue(os);
    os<<"c:"<<(isRed?"R":"B")<<" ";
}


#ifndef LICENCJAT_DICT_H
#define LICENCJAT_DICT_H

#include "Node.h"
#include <stdint.h>

class Dict {

public:
    virtual uint32_t search(const string &key) = 0;

    virtual bool insert(const string &key, const uint32_t &value)= 0;

    virtual bool remove(const string &key)= 0;

    uint32_t size = 0;

    Node *root = nullptr;

    virtual ~Dict() {
        delete root;
    }
};


#endif //LICENCJAT_DICT_H

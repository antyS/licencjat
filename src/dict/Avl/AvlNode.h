#ifndef LICENCJAT_AVLNODE_H
#define LICENCJAT_AVLNODE_H


#include "../Bst/BstNode.h"

class AvlNode : public BstNode {
public:
    char balanceFactor;
    AvlNode(const string &key, const uint32_t &val);


    void printNodeValue(ostream &os) const;
};


#endif //LICENCJAT_AVLNODE_H

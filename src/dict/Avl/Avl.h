#ifndef LICENCJAT_AVL_H
#define LICENCJAT_AVL_H


#include "../Bst/Bst.h"
#include "AvlNode.h"

class Avl: public Bst {
public:
    bool insert(const string &key, const uint32_t &value) override;

    bool remove(const string &key) override;


private:
    void repairFactorInsert(AvlNode *node);

    void repairFactorDelete(AvlNode *node, bool wasRightRemoved);
};


#endif //LICENCJAT_AVL_H

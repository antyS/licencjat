#include <tuple>
#include "Avl.h"

bool Avl::insert(const string &key, const uint32_t &value) {
    pair<BstNode *, bool> result = Bst::inserted(new AvlNode(key, value));
    if (result.second) {
        repairFactorInsert((AvlNode *) result.first);
    }
    return result.second;
}

void Avl::repairFactorInsert(AvlNode *node) {
    AvlNode *parent;
    AvlNode *child;
    for (; node->parent != nullptr; node = parent) {
        parent = (AvlNode *) node->parent;
        bool isRight = parent->right == node;
        if (isRight) {
            parent->balanceFactor--;
            if (parent->balanceFactor == 0)
                break;
            if (parent->balanceFactor == -1)
                continue;
            if (node->balanceFactor == -1) {
                node->balanceFactor = 0;
                parent->balanceFactor = 0;
                rotateLeft(parent);
                break;
            }
            child = (AvlNode *) node->left;
            parent->balanceFactor = 0;
            node->balanceFactor = 0;
            if (child->balanceFactor == -1)
                parent->balanceFactor = 1;
            else if (child->balanceFactor == 1)
                node->balanceFactor = -1;
            child->balanceFactor = 0;
            rotateRightLeft(parent);
            break;
        } else {
            parent->balanceFactor++;
            if (parent->balanceFactor == 0)
                break;
            if (parent->balanceFactor == 1)
                continue;
            if (node->balanceFactor == 1) {
                node->balanceFactor = 0;
                parent->balanceFactor = 0;
                rotateRight(parent);
                break;
            }
            child = (AvlNode *) node->right;
            parent->balanceFactor = 0;
            node->balanceFactor = 0;
            if (child->balanceFactor == 1)
                parent->balanceFactor = -1;
            else if (child->balanceFactor == -1)
                node->balanceFactor = 1;
            child->balanceFactor = 0;
            rotateLeftRight(parent);
            break;
        }
    }
}

bool Avl::remove(const string &key) {
    auto result = Bst::parentOfRemoved(key);
    if (get<1>(result)) {
        repairFactorDelete((AvlNode *) get<0>(result), get<2>(result));
    }
    return get<1>(result);
}

void Avl::repairFactorDelete(AvlNode *node, bool wasRightRemoved) {
    AvlNode *parent;
    AvlNode *child;
    AvlNode *grandChild;
    for (; node != nullptr; node = parent) {
        parent = (AvlNode *) node->parent;
        if (wasRightRemoved) {
            node->balanceFactor++;
            if (node->balanceFactor == 1)
                break;
            if (parent != nullptr && parent->left == node)
                wasRightRemoved = false;
            if (node->balanceFactor == 0)
                continue;
            child = (AvlNode *) node->left;
            if (child->balanceFactor == 0) {
                node->balanceFactor = 1;
                child->balanceFactor = -1;
                rotateRight(node);
                break;
            }
            if (child->balanceFactor == 1) {
                node->balanceFactor = 0;
                child->balanceFactor = 0;
                rotateRight(node);
                continue;
            }
            grandChild = (AvlNode *) child->right;
            node->balanceFactor = 0;
            child->balanceFactor = 0;
            if (grandChild->balanceFactor == 1)
                node->balanceFactor = -1;
            else if (grandChild->balanceFactor == -1)
                child->balanceFactor = 1;
            grandChild->balanceFactor = 0;
            rotateLeftRight(node);
        } else {
            node->balanceFactor--;
            if (node->balanceFactor == -1)
                break;
            if (parent !=nullptr && parent->right == node)
                wasRightRemoved = true;
            if (node->balanceFactor == 0)
                continue;
            child = (AvlNode *) node->right;
            if (child->balanceFactor == 0) {
                node->balanceFactor = -1;
                child->balanceFactor = 1;
                rotateLeft(node);
                break;
            }
            if (child->balanceFactor == -1) {
                node->balanceFactor = 0;
                child->balanceFactor = 0;
                rotateLeft(node);
                continue;
            }
            grandChild = (AvlNode *) child->left;
            node->balanceFactor = 0;
            child->balanceFactor = 0;
            if (grandChild->balanceFactor == -1)
                node->balanceFactor = 1;
            else if (grandChild->balanceFactor == 1)
                child->balanceFactor = -1;
            grandChild->balanceFactor = 0;
            rotateRightLeft(node);
        }
    }

}

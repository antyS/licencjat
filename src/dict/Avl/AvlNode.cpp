#include "AvlNode.h"


AvlNode::AvlNode(const string &key, const uint32_t &val) : BstNode(key, val) {
    this->balanceFactor = 0;
}

void AvlNode::printNodeValue(std::ostream &os) const {
    os<<"b:"<<balanceFactor;
    BstNode::printNodeValue(os);
}




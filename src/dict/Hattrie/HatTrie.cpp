#include "HatTrieNode.h"
#include "HatTrie.h"
#include "HashMap/HashMap.h"
#include "stdexcept"

uint32_t HatTrie::search(const string &key) {
    HatTrieNodeBase *actual = (HatTrieNodeBase *) root;
    for (uint32_t i = 0; actual != nullptr && i < key.size(); i++) {
        if (actual->getType() == HatTrieNodeType::CONTAINER) {
            return ((HashMap *) actual)->search(key.substr(i, key.size()));
        } else {
            actual = ((HatTrieNode *) actual)->nodes[key[i]];
        }
    }
    if (actual != nullptr) {
        return actual->val;
    }
    return NULL;
}

bool HatTrie::insert(const string &key, const uint32_t &value) {
    HatTrieNodeBase *actual = (HatTrieNodeBase *) root;
    HatTrieNode *parent = (HatTrieNode *) (actual);
    uint32_t i;
    for (i = 0; actual->getType() == HatTrieNodeType::INTERNAL && i < key.size(); i++) {
        parent = (HatTrieNode *) (actual);
        actual = ((HatTrieNode *) actual)->nodes[key[i]];
        if (actual == nullptr) {
            ++i;
            break;
        }
    }
    if (actual == nullptr) {
        auto *created = new HashMap(parent);
        parent->nodes[key[i - 1]] = created;
        parent->size++;
        if (i < key.size()) {
            created->insert(key.substr(i, key.size()), value);
        } else {
            created->val = value;
            created->size++;
        }
    } else if (i >= key.size()) {
        if (actual->val != NULL) {
            return false;
        }
        actual->val = value;
        actual->size++;
    } else if (actual->getType() == HatTrieNodeType::CONTAINER) {//not internal
        HashMap *hashMap = (HashMap *) actual;
        bool result = hashMap->insert(key.substr(i, key.size()), value);
        if (result) {
            size++;
            if (hashMap->size > 65536) {
                HatTrieNode *burst = hashMap->burst();
                if (hashMap == root) {
                    root = burst;
                } else {
                    parent->nodes[key[i - 1]] = burst;
                    burst->parent = parent;
                }
                delete hashMap;
            }
        }
        return result;
    } else {
        throw runtime_error("actual not container");
    }
    size++;
    return true;
}

bool HatTrie::remove(const string &key) {
    HatTrieNodeBase *actual = (HatTrieNodeBase *) root;
    uint16_t i;
    for (i = 0; actual->getType() == HatTrieNodeType::INTERNAL && i < key.size(); i++) {
        actual = ((HatTrieNode *) actual)->nodes[key[i]];
        if (actual == nullptr) {
            ++i;
            break;
        }
    }
    if (actual == nullptr) {
        return false;
    } else if (i == key.size()) {
        if (actual->val == NULL)
            return false;
        ((HatTrieNode *) actual)->val = NULL;
        actual->size--;
        fixRemove((HatTrieNode *) actual, key);
        size--;
        return true;
    } else if (actual->getType() == HatTrieNodeType::CONTAINER) {//not internal
        bool result = ((HashMap *) actual)->remove(key.substr(i, key.size()));
        if (result) {
            size--;
            if (((HashMap *) actual)->size <= 0 && actual != root) {
                fixRemove(actual, key.substr(0, i));
            }
        }
        return result;
    } else {
        throw runtime_error("actual not container");
    }
}

HatTrie::HatTrie() {
    root = new HashMap(nullptr);
}

void HatTrie::fixRemove(HatTrieNodeBase *node, const string &key) {
    HatTrieNodeBase *actual = node;
    HatTrieNode *parent;
    for (uint32_t i = (uint32_t) key.size() - 1; i >= 0 && actual != this->root; i--) {
        if (actual->size <= 0) {
            parent = (HatTrieNode *) actual->parent;
            parent->nodes[key[i]] = nullptr;
            parent->size--;
            delete actual;
            actual = parent;
        } else {
            break;
        }
    }
}

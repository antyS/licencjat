#ifndef LICENCJAT_HATTRIENODEBASE_H
#define LICENCJAT_HATTRIENODEBASE_H


#include "../../dict/Node.h"
#include "HatTrieNodeType.h"
#include "HatTrieNode.h"

class HatTrieNodeBase : public Node {

public:
    HatTrieNodeBase *parent = nullptr;
    uint32_t val = NULL;
    uint32_t size = 0;
    virtual HatTrieNodeType getType()= 0;
};

#endif //LICENCJAT_HATTRIENODEBASE_H

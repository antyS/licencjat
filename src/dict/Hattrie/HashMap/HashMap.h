#ifndef LICENCJAT_HASHNODE_H
#define LICENCJAT_HASHNODE_H


#include "../HatTrieNode.h"
#include "../../Dict.h"

using namespace std;


class HashMap : public HatTrieNodeBase {
    static uint32_t bucketIndex(const string &key);

    bool insert(const string &size, const string &key, const string &value);

    string buckets[512];
public:
    HashMap(HatTrieNode *parent);

    uint32_t search(const string &key);

    bool insert(const string &key, const uint32_t &value);

    bool remove(const string &key);

    HatTrieNode *burst();

    HatTrieNodeType getType() override;

    static string encodeInString(const string &key, const uint32_t &value);

    static uint16_t decodeSizeFromString(const string &bucket, uint32_t sizeStartPos);

    static uint32_t decodeValueFromString(const string &bucket, uint32_t valueStartPos);

    static string encodeSizeInString(uint16_t keySize);

    static string encodeValueInString(const uint32_t &value);
};


#endif //LICENCJAT_HASHNODE_H

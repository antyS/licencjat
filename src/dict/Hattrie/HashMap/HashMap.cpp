#include "HashMap.h"

uint32_t HashMap::bucketIndex(const string &key) {
    uint32_t seed = 220373;
    for (char c:key) {
        seed ^= ((seed << 5) + c + (seed >> 2));
    }
    return (seed & 0x7fffffff) & 511;
}

uint32_t HashMap::search(const string &key) {
    uint32_t index = bucketIndex(key);
    string bucket = buckets[index];
    if (bucket.empty()) {
        return NULL;
    }
    for (uint32_t i = 0; i < bucket.size();) {
        uint16_t size = decodeSizeFromString(bucket, i);
        i += 2;
        uint32_t oneAfterKeyEnd = i + size;
        if (size != key.size()) {
            i = oneAfterKeyEnd + 4;
        } else {
            for (int j = 0; j < key.size(); j++, i++) {
                if (key[j] != bucket[i]) {
                    i = oneAfterKeyEnd + 4;
                    break;
                }
            }
            if (i == oneAfterKeyEnd) {
                return decodeValueFromString(bucket, i);
            }
        }
    }
    return NULL;
}

bool HashMap::insert(const string &key, const uint32_t &value) {
    if (search(key) != NULL) {
        return false;
    }
    string toInsert = encodeInString(key, value);
    buckets[bucketIndex(key)] += toInsert;
    this->size++;
    return true;
}

bool HashMap::insert(const string &size, const string &key, const string &value) {
    string toInsert = size + key + value;
    buckets[bucketIndex(key)] += toInsert;
    this->size++;
    return true;
}

bool HashMap::remove(const string &key) {
    uint32_t index = bucketIndex(key);
    string &bucket = buckets[index];
    if (bucket.empty()) {
        return false;
    }
    for (uint32_t i = 0; i < bucket.size();) {
        uint16_t size = decodeSizeFromString(bucket, i);
        i += 2;
        uint32_t end = i + size;
        if (size != key.size()) {
            i = end + 4;
        } else {
            for (int j = 0; j < key.size(); j++, i++) {
                if (key[j] != bucket[i]) {
                    i = end + 4;
                    break;
                }
            }
            if (i == end) {
                bucket.erase(end - (size + 2), size + 6);
                this->size--;
                return true;
            }
        }
    }
    return false;
}

HatTrieNode *HashMap::burst() {
    auto *created = new HatTrieNode();
    for (auto &bucket : buckets) {
        for (uint32_t i = 0; i < bucket.size();) {
            uint16_t size = decodeSizeFromString(bucket, i);
            i += 2;
            if ((HashMap *) created->nodes[bucket[i]] == nullptr) {
                created->nodes[bucket[i]] = new HashMap(created);
                created->size++;
            }
            HashMap *hashMap = (HashMap *) (created->nodes[bucket[i]]);
            if (size == 1) {
                hashMap->val = decodeValueFromString(bucket, i + size);
                hashMap->size++;
            } else {
                hashMap->insert(encodeSizeInString(size - 1),
                               bucket.substr(i + 1, size - 1),
                               bucket.substr(i + size, 4));

            }
            i += size + 4;
        }
    }
    if (this->val != NULL) {
        created->val = this->val;
        created->size++;
    }
    return created;
}

HatTrieNodeType HashMap::getType() {
    return HatTrieNodeType::CONTAINER;
}

uint32_t HashMap::decodeValueFromString(const string &bucket, uint32_t valueStartPos) {
    return (uint32_t) ((uint8_t) (bucket[valueStartPos])
                       | ((uint8_t) (bucket[valueStartPos + 1]) << 8)
                       | ((uint8_t) (bucket[valueStartPos + 2]) << 16)
                       | ((uint8_t) (bucket[valueStartPos + 3]) << 24));
}

uint16_t HashMap::decodeSizeFromString(const string &bucket, uint32_t sizeStartPos) {
    return (uint16_t) ((uint8_t) (bucket[sizeStartPos]) |
                       (uint8_t) (bucket[sizeStartPos + 1]) << 8);
}

string HashMap::encodeInString(const string &key, const uint32_t &value) {
    auto sizeInString = encodeSizeInString(key.size());
    auto valueInString = encodeValueInString(value);
    return sizeInString + key + valueInString;

}

string HashMap::encodeValueInString(const uint32_t &value) {
    string toInsert = "aaaa";
    toInsert[0] = (value & 255);
    toInsert[1] = ((value >> 8) & 255);
    toInsert[2] = ((value >> 16) & 255);
    toInsert[3] = ((value >> 24) & 255);
    return toInsert;
}

string HashMap::encodeSizeInString(uint16_t keySize) {
    string toInsert = "aa";
    toInsert[0] = (keySize & 255);
    toInsert[1] = ((keySize >> 8) & 255);
    return toInsert;
}

HashMap::HashMap(HatTrieNode *parent) {
    this->parent = parent;
}


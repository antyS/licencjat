#ifndef LICENCJAT_HATTRIENODE_H
#define LICENCJAT_HATTRIENODE_H


#include <climits>
#include "HatTrieNodeBase.h"

using std::string;

class HatTrieNode : public HatTrieNodeBase {
public:
    HatTrieNodeBase *nodes[UCHAR_MAX + 1] = {nullptr};

    HatTrieNodeType getType() override;

};


#endif //LICENCJAT_HATTRIENODE_H

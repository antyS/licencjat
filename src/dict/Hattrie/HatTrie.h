#ifndef LICENCJAT_HAT_TRIE_H
#define LICENCJAT_HAT_TRIE_H


#include "../Dict.h"

class HatTrie: public Dict {
public:
    HatTrie();

    uint32_t search(const string &key) override;

    bool insert(const string &key, const uint32_t &value) override;

    bool remove(const string &key) override;

    void fixRemove(HatTrieNodeBase *node, const string &key);
};


#endif //LICENCJAT_HAT_TRIE_H

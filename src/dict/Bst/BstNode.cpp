#include <sstream>
#include "BstNode.h"

BstNode::BstNode(const string &key, const uint32_t &val) : key(key), val(val) {
    left = nullptr;
    right = nullptr;
    parent = nullptr;
}


void BstNode::printNodeValue(ostream &os) const {
    os << " k:" << key;
//    os<< " v:";
//    if (val == nullptr) {
//        os << "nullptr";
//    } else {
//        os << val;
//    }
    os << " ";
}

void BstNode::printTree(ostream &os, bool isRight, const string &indent) {
    if (right != nullptr) {
        right->printTree(os, true, indent + (isRight ? "        " : " |      "));
    }
    os << indent;
    if (isRight) {
        os << " /";
    } else {
        os << " \\";
    }
    os << "-----";
    printNodeValue(os);
    os << endl;
    if (left != nullptr) {
        left->printTree(os, false, indent + (isRight ? " |      " : "        "));
    }
}

string BstNode::toString() {
    stringstream os;
    if (right != nullptr) {
        right->printTree(os, true, "");
    }
    printNodeValue(os);
    os << endl;
    if (left != nullptr) {
        left->printTree(os, false, "");
    }
    return os.str();
}

BstNode::~BstNode() {
    delete this->right;
    this->right = nullptr;
    delete this->left;
    this->left = nullptr;
}

void BstNode::copyValues(BstNode *toMe) {
    toMe->key = key;
    toMe->val = val;
}


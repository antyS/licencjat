#ifndef LICENCJAT_BST_H
#define LICENCJAT_BST_H

#include "BstNode.h"
#include "../Dict.h"

class Bst : public Dict {
public:
    virtual ~Bst();

    uint32_t search(const string &key) override;

    virtual bool insert(const string &key, const uint32_t &value) override;

    virtual bool remove(const string &key) override;

protected:

    /**
 * @return 0  - parent of removed BstNode, or last searched
 * 1 - isRemoved
 * 2 - wasLeftRemoved
 */
    tuple<__decay_and_strip<BstNode *&>::__type, bool, bool> parentOfRemoved(const string &key);

/**
 * @return nullable requested or last processed BstNode
 */
    BstNode *searchOrLast(const string &key) const;

    pair<BstNode *, bool> inserted(BstNode *toInsert);

    void rotateLeft(BstNode *localRoot);

    void rotateLeftRight(BstNode *localRoot);

    void rotateRight(BstNode *localRoot);

    void rotateRightLeft(BstNode *localRoot);

    BstNode *minKey(BstNode *BstNode);

};

#endif //LICENCJAT_BST_H

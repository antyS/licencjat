#include <tuple>
#include "Bst.h"

uint32_t Bst::search(const string &key) {
    BstNode *last = searchOrLast(key);
    if (last != nullptr && last->key == key) {
        return last->val;
    }
    return NULL;
}

BstNode *Bst::searchOrLast(const string &key) const {
    BstNode *current = (BstNode *) root;
    BstNode *parent = (BstNode *) root;
    while (current != nullptr) {
        parent = current;
        if (current->key > key) {
            current = current->left;
        } else if (current->key < key) {
            current = current->right;
        } else {
            return current;
        }
    }
    return parent;
}

bool Bst::insert(const string &key, const uint32_t &value) {
    return inserted(new BstNode(key, value)).second;
}

pair<BstNode *, bool> Bst::inserted(BstNode *toInsert) {

    BstNode *parent = searchOrLast(toInsert->key);
    if (parent != nullptr && parent->key == toInsert->key) {
        delete toInsert;
        return pair<BstNode *, bool>(parent, false);
    }
    toInsert->parent = parent;
    if (parent == nullptr) {
        root =  toInsert;
    } else {
        if (toInsert->key < parent->key)
            parent->left =  toInsert;
        else
            parent->right =  toInsert;
    }
    size++;
    return pair<BstNode *, bool>(toInsert, true);
}

bool Bst::remove(const string &key) {
    return get<1>(parentOfRemoved(key));
}

tuple<BstNode *, bool, bool> Bst::parentOfRemoved(const string &key) {
    BstNode *toRemove = searchOrLast(key);
    if (toRemove == nullptr || toRemove->key != key) {
        return make_tuple(toRemove, false, false);
    }
    BstNode *reallyRemove = nullptr;
    BstNode *child = nullptr;
    bool wasRightRemoved = false;
    if ((toRemove->left == nullptr) || (toRemove->right == nullptr)) {
        reallyRemove = toRemove;
    } else {
        reallyRemove = minKey(toRemove->right);
    }
    if (reallyRemove->left != nullptr) {
        child = reallyRemove->left;
    } else {
        child = reallyRemove->right;
    }
    if (child != nullptr) {
        child->parent = reallyRemove->parent;
    }
    if (reallyRemove->parent == nullptr) {
        root = child;
    } else if (reallyRemove == reallyRemove->parent->left) {
        reallyRemove->parent->left = child;
    } else {
        reallyRemove->parent->right = child;
        wasRightRemoved = true;
    }
    if (reallyRemove != toRemove) {
        reallyRemove->copyValues(toRemove);
    }
    auto result = make_tuple(reallyRemove->parent, true, wasRightRemoved);
    reallyRemove->left=nullptr;
    reallyRemove->right=nullptr;
    delete reallyRemove;
    size--;
    return result;
}



Bst::~Bst() {
    delete root;
}


void Bst::rotateRight(BstNode *localRoot) {
    BstNode *newLocalRoot = localRoot->left;
    if (newLocalRoot != nullptr) {
        localRoot->left = newLocalRoot->right;
        if (newLocalRoot->right != nullptr)
            newLocalRoot->right->parent = localRoot;
        newLocalRoot->right = localRoot;
        newLocalRoot->parent = localRoot->parent;
    }
    if (localRoot->parent != nullptr) {
        if (localRoot->parent->left == localRoot) {
            localRoot->parent->left = newLocalRoot;
        } else {
            localRoot->parent->right = newLocalRoot;
        }
    } else {
        this->root = newLocalRoot;
    }
    localRoot->parent = newLocalRoot;
}

void Bst::rotateLeft(BstNode *localRoot) {
    BstNode *newLocalRoot = localRoot->right;
    if (newLocalRoot != nullptr) {
        localRoot->right = newLocalRoot->left;
        if (newLocalRoot->left != nullptr)
            newLocalRoot->left->parent = localRoot;
        newLocalRoot->left = localRoot;
        newLocalRoot->parent = localRoot->parent;
    }
    if (localRoot->parent != nullptr) {
        if (localRoot->parent->right == localRoot) {
            localRoot->parent->right = newLocalRoot;
        } else {
            localRoot->parent->left = newLocalRoot;
        }
    } else {
        this->root = newLocalRoot;
    }
    localRoot->parent = newLocalRoot;
}

void Bst::rotateLeftRight(BstNode *localRoot) {
    rotateLeft(localRoot->left);
    rotateRight(localRoot);
}

void Bst::rotateRightLeft(BstNode *localRoot) {
    rotateRight(localRoot->right);
    rotateLeft(localRoot);
}

BstNode *Bst::minKey(BstNode *BstNode) {
    while (BstNode->left != nullptr)
        BstNode = BstNode->left;
    return BstNode;
}

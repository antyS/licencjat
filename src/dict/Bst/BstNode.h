#ifndef LICENCJAT_BSTNODE_H
#define LICENCJAT_BSTNODE_H


#include "../Node.h"

class BstNode : public Node {
public:
    BstNode *left;
    BstNode *right;
    BstNode *parent;
    string key;
    uint32_t val;

    BstNode(const string &key, const uint32_t &val);

    virtual ~BstNode();

    virtual void printNodeValue(std::ostream &os) const ;

    void printTree(std::ostream &os, bool isRight, const string &indent) ;

    string toString() override;

    void copyValues(BstNode *toMe);
};

#endif //LICENCJAT_BSTNODE_H

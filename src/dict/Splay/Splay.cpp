#include <tuple>
#include "Splay.h"


bool Splay::insert(const string &key, const uint32_t &value) {
    pair<BstNode *, bool> result = Bst::inserted(new BstNode(key, value));
    splay(result.first);
    return result.second;
}

bool Splay::remove(const string &key) {
    auto parent = Bst::parentOfRemoved(key);
    splay(get<0>(parent));
    return get<1>(parent);
}

uint32_t Splay::search(const string &key) {
    BstNode *last = Bst::searchOrLast(key);
    splay(last);
    if (last != nullptr && last->key == key) {
        return last->val;
    }
    return NULL;
}

void Splay::splay(BstNode *node) {
    BstNode *parent;
    BstNode *grandParent;
    while (node != nullptr && node->parent != nullptr) {
        parent = node->parent;
        grandParent = parent->parent;
        if (parent->right == node) {
            if (grandParent == nullptr) {
                rotateLeft(parent);
            } else if (grandParent->right == parent) {
                rotateLeft(grandParent);
                rotateLeft(parent);
            } else {
                rotateLeft(parent);
                rotateRight(grandParent);
            }
        } else {
            if (grandParent == nullptr) {
                rotateRight(parent);
            } else if (grandParent->left == parent) {
                rotateRight(grandParent);
                rotateRight(parent);
            } else {
                rotateRight(parent);
                rotateLeft(grandParent);
            }
        }
    }
    if(node!=nullptr){
        root = node;
    }
}



#ifndef LICENCJAT_SPLAY_H
#define LICENCJAT_SPLAY_H


#include "../Avl/Avl.h"

class Splay : public Bst {
public:
    bool insert(const string &key, const uint32_t &value) override;

    bool remove(const string &key) override;

    uint32_t search(const string &key) override;

private:
    void splay(BstNode*node);

};


#endif //LICENCJAT_SPLAY_H

#ifndef LICENCJAT_TRIE_H
#define LICENCJAT_TRIE_H


#include "../Dict.h"
#include "TrieNodeBase.h"


class Trie : public Dict {

public:
    int nodeCount = 0;

    explicit Trie(TrieNodeBase *root);

    uint32_t search(const string &key) override;

    bool insert(const string &key, const uint32_t &value) override;

    bool remove(const string &key) override;

    pair<__decay_and_strip<TrieNodeBase *&>::__type, unsigned int> searchOrLast(const string &key) const;


};


#endif //LICENCJAT_TRIE_H

#include "TrieListNode.h"


TrieListNode::~TrieListNode() {
    for (auto child:children) {
        delete child.second;
    }
}

void TrieListNode::pinChildren(const char &i, TrieNodeBase *pNode) {
    TrieNodeBase::pinChildren(i, pNode);
    this->children.emplace_front(i, pNode);
}

void TrieListNode::unpinChild(const char &i) {
    TrieNodeBase::unpinChild(i);
    children.remove_if([&](pair<char, TrieNodeBase *> p) { return p.first == i; });
}

TrieNodeBase *TrieListNode::newInstance() {
    return new TrieListNode();
}

TrieNodeBase *TrieListNode::getChild(const char &c) {
    for (auto pair:children) {
        if (pair.first == c) {
            return pair.second;
        }
    }
    return nullptr;
}


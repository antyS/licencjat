#ifndef LICENCJAT_TRIELISTNODE_H
#define LICENCJAT_TRIELISTNODE_H


#include <forward_list>
#include "../../Node.h"
#include "../TrieNodeBase.h"

class TrieListNode : public TrieNodeBase {
public:
    forward_list<pair<char, TrieNodeBase *>> children;

    TrieNodeBase * getChild(const char &c) override;

    virtual ~TrieListNode();

    void pinChildren(const char &i, TrieNodeBase *pNode) override;

    void unpinChild(const char &i) override;

    TrieNodeBase *newInstance() override;
};


#endif //LICENCJAT_TRIELISTNODE_H

#ifndef LICENCJAT_TRIENODEBASE_H
#define LICENCJAT_TRIENODEBASE_H


#include "../Node.h"

class TrieNodeBase : public Node {

public:
    TrieNodeBase *parent = nullptr;
    uint32_t val = NULL;
    uint32_t childCount = 0;

    virtual void pinChildren(const char &i, TrieNodeBase *pNode) {
        this->childCount++;
    };

    virtual void unpinChild(const char &i) {
        this->childCount--;
    };

    virtual TrieNodeBase *getChild(const char &i) = 0;

    virtual TrieNodeBase *newInstance() = 0;

};


#endif //LICENCJAT_TRIENODEBASE_H

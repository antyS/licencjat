#include "TrieArrayNode.h"


TrieArrayNode::~TrieArrayNode() {
    for (auto child:children) {
        delete child;
    }
}

void TrieArrayNode::pinChildren(const char &i, TrieNodeBase *pNode) {
    TrieNodeBase::pinChildren(i, pNode);
    this->children[i] = pNode;
}

void TrieArrayNode::unpinChild(const char &i) {
    TrieNodeBase::unpinChild(i);
    this->children[i] = NULL;
}

TrieNodeBase *TrieArrayNode::getChild(const char &i) {
    return this->children[i];
}

TrieNodeBase *TrieArrayNode::newInstance() {
    return new TrieArrayNode();
}


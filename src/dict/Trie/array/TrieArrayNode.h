#ifndef LICENCJAT_TRIEARRAYNODE_H
#define LICENCJAT_TRIEARRAYNODE_H


#include <forward_list>
#include "../../Node.h"
#include "../../../Consts.h"
#include "../TrieNodeBase.h"

class TrieArrayNode : public TrieNodeBase {
public:
    TrieNodeBase *children[MAX_CHAR + 1] = {nullptr};

    virtual ~TrieArrayNode();

    void pinChildren(const char &i, TrieNodeBase *pNode) override;

    void unpinChild(const char &i) override;

    TrieNodeBase *getChild(const char &i) override;

    TrieNodeBase *newInstance() override;
};


#endif //LICENCJAT_TRIEARRAYNODE_H

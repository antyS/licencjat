#include "Trie.h"

uint32_t Trie::search(const string &key) {
    auto orLast = searchOrLast(key);
    if (orLast.second == key.size()) {
        return orLast.first->val;
    }
    return NULL;
}

/**
 *
 * @param key
 * @return second - number of matched chars from key
 */
pair<__decay_and_strip<TrieNodeBase *&>::__type, unsigned int> Trie::searchOrLast(const string &key) const {
    auto *actual = (TrieNodeBase *) root, *parent = (TrieNodeBase *) root;
    uint32_t i;
    for (i = 0; actual != nullptr && i < key.size(); i++) {
        parent = actual;
        actual = actual->getChild(key[i]);
    }
    if (actual != nullptr) {
        return make_pair(actual, i);
    } else {
        return make_pair(parent, i - 1);
    }
}

bool Trie::insert(const string &key, const uint32_t &value) {
    auto orLast = searchOrLast(key);
    TrieNodeBase *actual = orLast.first;
    for (int i = orLast.second; i < key.size(); i++) {
        auto *created = ((TrieNodeBase *) root)->newInstance();
        nodeCount++;
        created->parent = actual;
        actual->pinChildren(key[i], created);
        actual = created;
    }
    if (actual->val == NULL) {
        actual->val = value;
        size++;
        return true;
    }
    return false;
}

bool Trie::remove(const string &key) {
    auto orLast = searchOrLast(key);
    if (orLast.second != key.size() || orLast.first->val == NULL) {
        return false;
    }
    TrieNodeBase *actual = orLast.first;
    actual->val = NULL;
    TrieNodeBase *parent;
    for (int i = (int) key.size() - 1; i >= 0; i--) {
        if (actual->childCount <= 0 && actual->val == NULL) {
            parent = actual->parent;
            parent->unpinChild(key[i]);
            delete actual;
            nodeCount--;
            actual = parent;
        } else {
            break;
        }
    }
    size--;
    return true;
}

Trie::Trie(TrieNodeBase *root) {
    this->root = root;
}


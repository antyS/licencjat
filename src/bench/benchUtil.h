#ifndef LICENCJAT_BENCHUTIL_H
#define LICENCJAT_BENCHUTIL_H

#include <iostream>
#include <random>
#include "../measurment/MeasureInfo.h"
#include "../measurment/MeasureHelper.h"
#include "../common/libs/json.hpp"
#include "../dict/Dict.h"
#include "../common/Utils.h"
#include "../Consts.h"

using json = nlohmann::json;
//json keys

default_random_engine *generator = new minstd_rand0(Utils::getSeed());

json benchDataSet(vector<string> dataSet, vector<string> &namesOfDictToBench);

MeasureInfo benchInsert(Dict *dict, std::vector<std::string> &dataSet);

MeasureInfo benchFind(Dict *dict, std::vector<std::string> &dataSet);

MeasureInfo benchMixed(Dict *dict, std::vector<std::string> &dataSet);

MeasureInfo benchRemove(Dict *dict, std::vector<std::string> &dataSet);

json benchDataSet(vector<string> dataSet, vector<string> &namesOfDictToBench) {
    std::vector<std::pair<std::string, Dict *>> dicts = Utils::generateDicts(namesOfDictToBench);
    json results;
    int id = Utils::generateTimeBasedId();
    for (auto pair:dicts) {
        json dictResult;
        dictResult[INSERT] = benchInsert(pair.second, dataSet).toJson();
        dictResult[FIND] = benchFind(pair.second, dataSet).toJson();
        dictResult[MIXED] = benchMixed(pair.second, dataSet).toJson();
        dictResult[REMOVE] = benchRemove(pair.second, dataSet).toJson();
        for(auto &item :dictResult){
            item[BENCH_ID] = id;
        }
        results[pair.first] = dictResult;
        delete pair.second;
    }
    return results;
}

MeasureInfo benchInsert(Dict *dict, std::vector<std::string> &dataSet) {
    return MeasureHelper::measureTimeAndMemory([&]() -> void {
        for (const auto &s:dataSet) {
            dict->insert(s, Utils::genVal());
        }
    });
}

MeasureInfo benchFind(Dict *dict, std::vector<std::string> &dataSet) {
    std::uniform_int_distribution<> urand(0, dataSet.size() - 1);
    return MeasureHelper::measureTimeAndMemory([&]() -> void {
        for (int i = 0; i < dataSet.size(); ++i) {
            dict->search(
                    dataSet.at(static_cast<unsigned long>(
                                       urand.operator()(*generator))));
        }
    });
}

MeasureInfo benchMixed(Dict *dict, std::vector<std::string> &dataSet) {
    std::uniform_int_distribution<> urand(0, dataSet.size() - 1);
    long opNum;
    return MeasureHelper::measureTimeAndMemory([&]() -> void {
        for (int i = 0; i < dataSet.size(); ++i) {
            auto index = static_cast<unsigned long>(urand.operator()(*generator));
            opNum = random() % 3;
            if (opNum == 0) {
                dict->search(dataSet.at(index));
            } else if (opNum == 1) {
                dict->insert(dataSet.at(index), Utils::genVal());
            } else if (opNum == 2) {
                dict->remove(dataSet.at(index));
            }
        }
    });
}

MeasureInfo benchRemove(Dict *dict, std::vector<std::string> &dataSet) {
    std::uniform_int_distribution<> urand(0, dataSet.size() - 1);
    shuffle(dataSet.begin(), dataSet.end(), *generator);
    return MeasureHelper::measureTimeAndMemory([&]() -> void {
        for (const auto &s : dataSet) {
            dict->remove(s);
        }
    });
}


#endif //LICENCJAT_BENCHUTIL_H

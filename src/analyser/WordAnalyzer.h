#ifndef LICENCJAT_WORDANALIZER_H
#define LICENCJAT_WORDANALIZER_H


#include <vector>
#include <string>
#include <cmath>
#include <iostream>
#include "../dict/Trie/Trie.h"
#define ALPHABET_SIZE 256

using namespace std;

class WordAnalyzer {

public:

    static double skewnessFactorByTrie(vector<string> &words);

    static double skewnessFactorByLoop(vector<string> &words);

    static double skewnessFactor(const vector<string> &words, double sum);

    static double calcAlphabetSizeBySkewnessFactor(double factor, const vector<string> &words, double avgPrefixLen);

    static int calcAlphabetSize(const vector<string> &words);

    static int sumByTrie(const vector<string> &words);

    static const int prefixLen(string w, string w1);

    static int sumByLoop(const vector<string> &words);

};


#endif //LICENCJAT_WORDANALIZER_H

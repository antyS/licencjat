#include "WordAnalyzer.h"
#include "../dict/Trie/array/TrieArrayNode.h"

double WordAnalyzer::skewnessFactorByTrie(vector<string> &words) {
    double sum = sumByTrie(words);
    return skewnessFactor(words, sum);
}

double WordAnalyzer::skewnessFactorByLoop(vector<string> &words) {
    double sum = sumByLoop(words);
    return skewnessFactor(words, sum);
}

double WordAnalyzer::skewnessFactor(const vector<string> &words, double sum) {
    double avgPrefixLen = sum / (words.size() - 1);
    double randomAvgPrefixLenPlusOne = log(words.size()) / log(ALPHABET_SIZE);
    double factor = (avgPrefixLen + 1) / randomAvgPrefixLenPlusOne;
    return factor;
}

double WordAnalyzer::calcAlphabetSizeBySkewnessFactor(double factor, const vector<string> &words, double avgPrefixLen) {
    return exp((factor * log(words.size())) / (avgPrefixLen + 1));
}

int WordAnalyzer::calcAlphabetSize(const vector<string> &words) {
    vector<bool> chars(1024, false);
    int result = 0;
    for (auto const &w:words) {
        for (unsigned char c:w) {
            if (!chars[c]) {
                result++;
                chars[c] = true;
            }
        }
    }
    return result;
}

int WordAnalyzer::sumByTrie(const vector<string> &words) {
    Trie trie(new TrieArrayNode());
    int sum = 0;
    for (auto const &w:words) {
        sum += trie.searchOrLast(w).second;
        trie.insert(w, 100);
    }
    return sum;
}

int WordAnalyzer::sumByLoop(const vector<string> &words) {
    int sum = 0;
    for (int i = 1; i < words.size(); ++i) {
        int maximum = 0;
        for (int j = i - 1; j >= 0; --j) {
            maximum = max(maximum, prefixLen(words[i], words[j]));
        }
        sum += maximum;
    }
    return sum;
}

const int WordAnalyzer::prefixLen(string w, string w1) {
    int len = 0;
    for (int i = 0; i < w.size(); ++i) {
        if (w[i] == w1[i]) {
            len++;
        } else
            break;
    }
    return len;
}

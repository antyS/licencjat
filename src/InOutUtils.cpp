#include "MainUtils.h"
#include <fstream>
#include <iostream>
#include "InOutUtils.h"
#include "Consts.h"

void InOutUtils::saveJson(const json &toSave, string &filename) {
    string filenameWithJsonAndDir = pathToJson(filename);
    fstream file;
    openFile(file, filenameWithJsonAndDir, ios_base::out | ios_base::trunc);
    file << toSave.dump();
    file.close();
    cout << "result saved to " << filenameWithJsonAndDir << endl;
}

string InOutUtils::pathToJson(const string &filename) {
    auto withDir = BENCH_RESULTS_DIR + filename;
    auto withExtension = has_suffix(withDir, ".json") ? withDir : withDir + ".json";
    return withExtension;
}

bool InOutUtils::has_suffix(const std::string &str, const std::string &suffix) {
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

void InOutUtils::openFile(fstream &file, string &filename, const ios_base::openmode __mode) {
    file.exceptions(ios_base::badbit);//ofstream::failbit |
    try {
        file.open(filename, __mode);
    } catch (const ios_base::failure &e) {
        cout << "failed to open file with filename: " << filename << " mode: " << __mode <<
             endl;
        cout << e.what() << endl;
        throw e;
    }
}

json InOutUtils::readJson(string &filename) {
    string filenameWithJsonAndDir = pathToJson(filename);
    fstream file;
    openFile(file, filenameWithJsonAndDir, ios_base::in);
    json result;
    file >> result;
    return result;
}

bool InOutUtils::is_file_exist(const string &fileName) {
    std::ifstream infile(fileName);
    return infile.good();
}
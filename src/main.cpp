#include <iostream>
#include "common/libs/cxxopts.hpp"
#include "Consts.h"
#include "MainUtils.h"

using namespace std;


int showHelp(const char *const *argv, const cxxopts::Options &options);


int main(int argc, const char *argv[]) {
    set_terminate([]() {
        cout << "Unhandled exception occurred\n";
        cout.flush();
        abort();
    });
    cxxopts::Options options(argv[0], "");
    try {
        stringstream customHelp;
        customHelp << MODE_PARAM << " " << JSON_PARAM << "\n";
        stringstream positional_help;
        positional_help << MODE_PARAM << ": \n\t" << BENCHMARK << " or " << GENERATE << "\n";
        positional_help << " " << JSON_PARAM << ": \n\t name of json file containing:\n\t generated dataset statistics"
                                                "\n\t benchmark results for specific dataset";
        options
                .custom_help(customHelp.str())
                .positional_help(positional_help.str())
                .show_positional_help();

        options.add_options()
                (HELP_PARAM, "Print this help");
        options.add_options(BENCHMARK)
                (DICT_PARAM,
                 "name of dictionary to bench, available dictionary_names: " + BST + "," + AVL + "," + RB + "," +
                 SPLAY + "," + TRIE_ARRAY + "," + HASH_MAP + "," + HAT_TRIE,
                 cxxopts::value<string>(), "dictionary_names...")
                (INPUT_PARAM, "name of input file containing words",
                 cxxopts::value<string>()->default_value(DATA_SET_NAME), FILE_NAME);
        options.add_options(GENERATE)
                (AVG_LEN_PARAM, "average length of words [1-50]",
                 cxxopts::value<unsigned int>(), "num")
                (WORD_COUNT_PARAM, "words count [1-]",
                 cxxopts::value<unsigned int>(), "num")
                (SKEWNESS_PARAM, "skewness (non randomenss) of words[1-100]",
                 cxxopts::value<unsigned int>(), "num")
                (OUTPUT_PARAM, "name of output file containing words",
                 cxxopts::value<string>()->default_value(DATA_SET_NAME), FILE_NAME);
        options.add_options("hidden")
                (MODE_PARAM, "", cxxopts::value<string>())
                (JSON_PARAM, "", cxxopts::value<string>());

        options.parse_positional({MODE_PARAM, JSON_PARAM});
        auto result = options.parse(argc, argv);

        if (result.count(HELP_PARAM)) {
            showHelp(argv, options);
            exit(0);
        }

        auto mode_parsed = result[MODE_PARAM].as<string>();
        if (mode_parsed == GENERATE) {
            auto avgWordLen = result[AVG_LEN_PARAM].as<unsigned int>();
            auto wordCount = result[WORD_COUNT_PARAM].as<unsigned int>();
            auto skewness = result[SKEWNESS_PARAM].as<unsigned int>();
            auto outputFilename = result[OUTPUT_PARAM].as<string>();
            auto jsonStatsFilename = result[JSON_PARAM].as<string>();

            MainUtils::genAndSaveDataSet(outputFilename, jsonStatsFilename, avgWordLen, wordCount, skewness);

        } else if (mode_parsed == BENCHMARK) {
            auto dictsToBench = result[DICT_PARAM].as<string>();
            auto inputFilename = result[INPUT_PARAM].as<string>();
            auto outputJsonFilename = result[JSON_PARAM].as<string>();
            MainUtils::benchmark(inputFilename, outputJsonFilename, {dictsToBench});
        } else {
            cout << "not supported mode" << endl;
            showHelp(argv, options);
            exit(1);
        }

    } catch (const cxxopts::OptionException &e) {
        cout << "error parsing options: " << e.what() << endl;
        showHelp(argv, options);
        cout.flush();
        exit(1);
    } catch (const exception &e) {
        cout << "unexpected exception with message occurred: " << e.what();
        cout.flush();
        exit(1);
    } catch (...) {
        cout << "unexpected exception of type not inherited from exception";
        cout.flush();
        exit(1);
    }
    return 0;
}

int showHelp(const char *const *argv, const cxxopts::Options &options) {
    cout << options.help({BENCHMARK, GENERATE, ""}) << endl;
    cout << "Examples:\n"
         << string(argv[0]) << " " << BENCHMARK << " jResults --" << DICT_PARAM << "=bst\n"
         << string(argv[0]) << " " << GENERATE << " jResults --" << AVG_LEN_PARAM << "=30 --"
         << WORD_COUNT_PARAM << "=1000 --" << SKEWNESS_PARAM << "=50"
         << endl;
}



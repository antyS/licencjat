#include "CharProbabilityDescriptor.h"

CharProbabilityDescriptor::CharProbabilityDescriptor(const string &sortedAlphabet,
                                                     const vector<double> &probabilityWeights) : sortedAlphabet(
        sortedAlphabet) {
    this->distribution = discrete_distribution<int>(probabilityWeights.begin(), probabilityWeights.end()
    );
}

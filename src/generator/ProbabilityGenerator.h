#ifndef LICENCJAT_WEIGHTGENERATOR_H
#define LICENCJAT_WEIGHTGENERATOR_H


#include <cmath>
#include <vector>
#include "CharProbabilityDescriptor.h"

using namespace std;

class ProbabilityGenerator {
public:
    /**
     * @param alphabet if alphapet size is small(<100) probability can get larger than supplied weight
     * @returns set of wordLen CharProbabilityDescriptors
     * @firstCharProbabilityDescriptor will have first letter probability = firstWeightBegin<p>
     * every i-th letter will have probability = (1-(sum from 0 to i over probabilities))*firstOthers
     * @lastCharProbabilityDescriptor follow the same rule as firstCharProbabilityDescriptor but with
     * parameters lastWeightBegin and lastOthers
     * @jThCharProbabilityDescriptor
     * jThWeightBegin follow linear function
     * between firstWeightBegin and lastWeightBegin<p>
     * jThOthers behave analogicaly as jThWeightBegin
     */
    static vector<CharProbabilityDescriptor>
        generateProbabilities(unsigned int wordLen, string alphabet, double firstWeightBegin,
                          double firstOthers, double lastWeightBegin, double lastOthers);

private:
    static void suggestAscending(double first, double last);
};


#endif //LICENCJAT_WEIGHTGENERATOR_H

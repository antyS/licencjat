#include <algorithm>
#include <iostream>
#include "ProbabilityGenerator.h"
#include "../../test/common/TestUtils.h"


vector<CharProbabilityDescriptor>
ProbabilityGenerator::generateProbabilities(unsigned int wordLen, string alphabet, double firstWeightBegin,
                                       double firstOthers, double lastWeightBegin, double lastOthers) {
    TestUtils::assertPercentageFromZeroToHundred({firstWeightBegin, firstOthers, lastWeightBegin, lastOthers});
    suggestAscending(firstWeightBegin, lastWeightBegin);
    suggestAscending(firstOthers, lastOthers);
    vector<CharProbabilityDescriptor> result = vector<CharProbabilityDescriptor>();
    result.reserve(wordLen);
    double weightBeginStep = (firstWeightBegin - lastWeightBegin) / (wordLen - 1);
    double othersStep = (firstOthers - lastOthers) / (wordLen - 1);
    for (int i = 0; i < wordLen; ++i) {
        vector<double> weights = vector<double>();
        weights.reserve(alphabet.size());
        double others = firstOthers - othersStep * i;
        double weightBegin = firstWeightBegin - weightBeginStep * i;

        weights.push_back(weightBegin);
        double sum = weightBegin;
        for (int j = 1; j < alphabet.size(); ++j) {
            double w = (1.0 - sum) * others;
            weights.push_back(w);
            sum += w;
        }
        random_shuffle(alphabet.begin(), alphabet.end()); // NOLINT
        result.emplace_back(alphabet, weights);
    }
    return result;
}

void ProbabilityGenerator::suggestAscending(double first, double last) {
    if (first < last) {
        cout << "consider ascending probabilities "
                "to generate strings with common prefixes not suffixes" << endl;
    }
}


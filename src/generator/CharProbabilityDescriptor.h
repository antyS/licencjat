#ifndef LICENCJAT_WORDLETTER_H
#define LICENCJAT_WORDLETTER_H

#include <string>
#include <vector>
#include <random>

using namespace std;


class CharProbabilityDescriptor {
public:
    //used via emplace_back
    CharProbabilityDescriptor(const string &sortedAlphabet, const vector<double> &probabilityWeights);

    /**
     * alphabet sorted desc by probability
     */
    string sortedAlphabet;
    /**
     * descending array containing weights of probabilities
     * for every char in sortedAlphabet
     */
    discrete_distribution<int> distribution;
};


#endif //LICENCJAT_WORDLETTER_H

#include <cassert>
#include <locale>
#include "WordGenerator.h"
#include "../../test/common/TestUtils.h"

WordGenerator::WordGenerator(unsigned int avgWordLen, unsigned int alphabetSize, double skewness) {
    TestUtils::assertPercentageFromZeroToHundred({skewness});
    this->alphabet = constructAlphabet(alphabetSize);
    this->wordLenDistribution = new binomial_distribution<int>(avgWordLen * 2, 0.5);
    probableChars = ProbabilityGenerator::generateProbabilities(static_cast<unsigned int>(wordLenDistribution->max()),
                                                                alphabet, 0.98 * skewness, 0.6 * skewness,
                                                                0.3 * skewness,
                                                                0.1 * skewness);
}

vector<string> WordGenerator::generateSkewed(unsigned int wordsCount) {
    vector<string> result;
    for (int i = 0; i < wordsCount; ++i) {
        result.push_back(generateSingleWordSkewed());
    }
    return result;
}


string WordGenerator::generateSingleWordSkewed() const {
    string word;
    int wordLen = wordLenDistribution->operator()(*generator);
    for (unsigned int i = 0; i < wordLen; ++i) {
        discrete_distribution<int> distribution = this->probableChars[i].distribution;
        int d = distribution.operator()(*generator);
        word += (char) this->probableChars[i].sortedAlphabet[d];
        distribution.reset();//so chars in different words at the same position are independent
    }
    return word;
}

string WordGenerator::constructAlphabet(unsigned int size) {
    string result = R"(abcdefghijklmnopqrstuvwxyz{|}~!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`)";
    result = result.substr(0, std::min(size, (unsigned int) result.size()));
    return result;
}

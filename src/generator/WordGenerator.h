#ifndef LICENCJAT_WORDGENERATOR_H
#define LICENCJAT_WORDGENERATOR_H


#include "CharProbabilityDescriptor.h"
#include "ProbabilityGenerator.h"
#include "../common/Utils.h"
#include "../Consts.h"
#include <string>
#include <vector>
#include <algorithm>
#include <random>

using namespace std;

class WordGenerator {
    string alphabet;
    default_random_engine *generator = new minstd_rand0(Utils::getSeed());
    uniform_int_distribution<int> random = uniform_int_distribution<int>(0, ALPHABET_SIZE - 1);
    //for all char in avgWordLen
    vector<CharProbabilityDescriptor> probableChars;

    binomial_distribution<int>* wordLenDistribution;

public:
    /**
     * 
     * @param skewness 0.1 to 1.0, the smaller the more random words are
     * 
     */
    explicit WordGenerator(unsigned int avgWordLen, unsigned int alphabetSize, double skewness);

    virtual ~WordGenerator() {
        delete generator;
    }

    vector<string> generateSkewed(unsigned int wordsCount);

    string generateSingleWordSkewed() const;

    /**
     * No null, no whitespaces
     * @return max 120 chars,
     */
    static string constructAlphabet(unsigned int size);
};


#endif //LICENCJAT_WORDGENERATOR_H

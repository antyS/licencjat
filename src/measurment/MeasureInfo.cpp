#include "MeasureInfo.h"


MeasureInfo::MeasureInfo(const MemoryInfo &info, const std::chrono::nanoseconds &duration) : memoryInfo(info),
                                                                                             duration(duration) {}

std::ostream &operator<<(std::ostream &os, const MeasureInfo &info) {
    os << "memory: " << info.memoryInfo << " \nduration: " << MeasureInfo::readableTime(info.duration);
    return os;
}

string MeasureInfo::readableTime(chrono::nanoseconds nanos) {
    stringstream ss;
    ss << setfill('0');
    auto hours = duration_cast<chrono::hours>(nanos);
    nanos -= hours;
    skipIfZero(ss, hours.count(), "h:");
    auto minutes = duration_cast<chrono::minutes>(nanos);
    nanos -= minutes;
    skipIfZero(ss, minutes.count(), "m:");
    auto seconds = duration_cast<chrono::seconds>(nanos);
    nanos -= seconds;
    skipIfZero(ss, seconds.count(), "s:");
    auto milliseconds = duration_cast<chrono::milliseconds>(nanos);
    nanos -= milliseconds;
    ss << setw(1) << milliseconds.count() << "ms";
    return ss.str();
}

stringstream &MeasureInfo::skipIfZero(stringstream &ss, long count, const string &unit) {
    if (count > 0) {
        ss << setw(1) << count << unit;
    }
    return ss;
}


json MeasureInfo::toJson() {
    json result;
    result[DURATION] = this->duration.count();
    result[MEMORY_INFO] = this->memoryInfo.toJson();
    return result;
}

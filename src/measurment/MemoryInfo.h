#ifndef LICENCJAT_MEMORYINFO_H
#define LICENCJAT_MEMORYINFO_H

#include <ostream>
#include <chrono>
#include "../common/libs/json.hpp"

#define VM_SIZE "vmSize"
#define VM_RSS "vmRSS"
#define VM_PEAK "vmPeak"
#define VM_HWM "vmHWM"
#define AVG_VM_SIZE "avgVmSize"
#define AVG_VM_RSS "avgVmRSS"

using namespace std;
using namespace chrono;
using json = nlohmann::json;

/**
 * same as in /proc/[pid]/status
 * + averages
 */
class MemoryInfo {
public:
    //if multiple memory measures occured,
    //value from last measurment will be used
    unsigned long vmSize;
    unsigned long vmRSS;
    //peak vmSize
    unsigned long vmPeak;
    //peak vmRSS
    unsigned long vmHWM;

    double avgVmSize = 0.0;
    double avgVmRSS = 0.0;

    MemoryInfo(unsigned long vmPeak, unsigned long vmSize, unsigned long vmHWM, unsigned long vmRSS);

    friend ostream &operator<<(ostream &os, const MemoryInfo &info);

    json toJson();
};

#endif //LICENCJAT_MEMORYINFO_H

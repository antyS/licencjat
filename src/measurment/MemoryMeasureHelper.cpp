#include <fstream>
#include <sstream>
#include <vector>
#include <thread>

#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnusedImportStatement"
//#include <unistd.h>
#include <unistd.h>//noinspection
#include "MemoryMeasureHelper.h"
#include <iostream>
//#define DEBUG_INFO_MEMORY_MEASURE
using namespace std;

MemoryInfo measure(int &numberOfMeasures, MemoryInfo &withAvg);

MemoryInfo measureMemory() {
    pid_t mypid = getpid();
    stringstream path;
    path << "/proc/" << to_string(mypid) << "/status";
    ifstream status{path.str()};
    if (not(status.is_open() and status.good())) {
        throw runtime_error("Couldn't open file " + path.str());
    }
    unsigned long memSizes[4] = {0};
    vector<string> memSizesNames = {"VmPeak", "VmSize", "VmHWM", "VmRSS"};
    string line, rubbish;
    while (std::getline(status, line)) {
        std::istringstream iss(line);
        for (int i = 0; i < memSizesNames.size(); ++i) {
            if (line.find(memSizesNames[i]) != string::npos) {
                iss >> rubbish >> memSizes[i];
#ifdef DEBUG_INFO_MEMORY_MEASURE
                cout << line << endl;
#endif
            }
        }
    }
    status.close();
    return MemoryInfo(memSizes[0], memSizes[1], memSizes[2], memSizes[3]);
}


MemoryInfo measureMemoryWithAvg(atomic<bool> &stillMeasure, uint8_t everyNSec) {
    MemoryInfo avg = MemoryInfo(0, 0, 0, 0);
    int numberOfMeasures = 0;
    measure(numberOfMeasures, avg);
    while (stillMeasure) {
        this_thread::sleep_for(chrono::seconds(everyNSec));
        if (stillMeasure)
            measure(numberOfMeasures, avg);
    }
    MemoryInfo last = measure(numberOfMeasures, avg);
    last.avgVmRSS = avg.avgVmRSS;
    last.avgVmSize = avg.avgVmSize;
    return last;
}

MemoryInfo measure(int &numberOfMeasures, MemoryInfo &withAvg) {
    MemoryInfo meas = measureMemory();
    numberOfMeasures++;
    withAvg.avgVmSize += (meas.vmSize - withAvg.avgVmSize) / numberOfMeasures;
    withAvg.avgVmRSS += (meas.vmRSS - withAvg.avgVmRSS) / numberOfMeasures;
    return meas;
}

#pragma clang diagnostic pop
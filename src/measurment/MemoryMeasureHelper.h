#ifndef LICENCJAT_MEMORYMEASUREHELPER_H
#define LICENCJAT_MEMORYMEASUREHELPER_H

#include <atomic>
#include "MemoryInfo.h"

using namespace std;

MemoryInfo measureMemory();

MemoryInfo measureMemoryWithAvg(atomic<bool> &stillMeasure, uint8_t everyNSec = 5);


#endif //LICENCJAT_MEMORYMEASUREHELPER_H

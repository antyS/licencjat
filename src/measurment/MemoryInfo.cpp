#include <sstream>
#include <iomanip>
#include "MemoryInfo.h"

MemoryInfo::MemoryInfo(unsigned long vmPeak,
                       unsigned long vmSize,
                       unsigned long vmHWM,
                       unsigned long vmRSS) {
    this->vmPeak= vmPeak;
    this->vmSize = vmSize;
    this->vmHWM = vmHWM;
    this->vmRSS = vmRSS;
}

std::ostream &operator<<(std::ostream &os, const MemoryInfo &info) {
    os << "vmPeak: " << info.vmPeak << " kb vmSize: " << info.vmSize << " kb vmHWM: " << info.vmHWM << " kb vmRSS: "
       << info.vmRSS << " kb avgVmSize: " << info.avgVmSize << " kb avgVmRSS: " << info.avgVmRSS << " kb";
    return os;
}

json MemoryInfo::toJson() {
    return {
            {VM_SIZE,     this->vmSize},
            {VM_RSS,      this->vmRSS},
            {VM_PEAK,     this->vmPeak},
            {VM_HWM,      this->vmHWM},
            {AVG_VM_SIZE, this->avgVmSize},
            {AVG_VM_RSS,  this->avgVmRSS},
    };
}



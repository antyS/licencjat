#ifndef LICENCJAT_MEASUREHELPER_H
#define LICENCJAT_MEASUREHELPER_H


#include <functional>
#include "MeasureInfo.h"

class MeasureHelper {
public:
    static MeasureInfo measureTimeAndMemory(const function<void()> &func);

};


#endif //LICENCJAT_MEASUREHELPER_H

#include <atomic>
#include <future>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "MeasureHelper.h"
#include "MemoryMeasureHelper.h"

using namespace std::chrono;

MeasureInfo MeasureHelper::measureTimeAndMemory(const function<void()> &func) {
    chrono::time_point<system_clock, chrono::nanoseconds> start, stop;
    std::atomic<bool> stillMeasure(true);
    auto memInfo = async(measureMemoryWithAvg, ref(stillMeasure), 5);
    start = high_resolution_clock::now();
    func();
    stop = high_resolution_clock::now();
//    breaks memory measurment in async
    stillMeasure = false;
    return MeasureInfo(memInfo.get(), duration_cast<chrono::nanoseconds>(stop - start));
}


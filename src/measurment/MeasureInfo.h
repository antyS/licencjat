#ifndef LICENCJAT_MEASUREINFO_H
#define LICENCJAT_MEASUREINFO_H


#include <chrono>
#include <ostream>
#include <sstream>
#include <iomanip>
#include "MemoryInfo.h"
#include "../common/libs/json.hpp"

#define DURATION "duration-ns"
#define MEMORY_INFO "memoryInfo-kB"


using namespace std;
using json = nlohmann::json;

class MeasureInfo {
public:
    MemoryInfo memoryInfo;
    chrono::nanoseconds duration;

    static string readableTime(chrono::nanoseconds nanos);

    MeasureInfo(const MemoryInfo &info, const chrono::nanoseconds &duration);

    friend ostream &operator<<(ostream &os, const MeasureInfo &info);

    json toJson();
    
private:

    static stringstream &skipIfZero(stringstream &ss, long count, const string &unit);
};


#endif //LICENCJAT_MEASUREINFO_H

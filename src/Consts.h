#ifndef LICENCJAT_CONSTS_H
#define LICENCJAT_CONSTS_H

#include "string"

#define ALPHABET_SIZE 94
#define MAX_CHAR 126

using namespace std;

static string BENCH_ID = "benchId";
static string DATASET_ID = "datasetId";
static string BENCH = "bench";
static string WORD_COUNT = "wordCount";
static string SKEWNESS = "skewness";
static string CHARS_COUNT = "charCount";
static string WORDS_STORAGE_BYTES = "wordsStorageBytes";
static string WORDS_END_COUNT = "wordsEndCount";
static string AVG_WORD_LEN = "avgWordLen";
static string DATASET_STATS = "datasetStats";
static string BENCH_RESULTS_DIR = "benchResults/";
static string FILE_NAME = "fileName";
static string HELP_PARAM = "help";
static string DATA_SET_NAME = "dataSet.txt";
static string GENERATE = "generate";
static string BENCHMARK = "benchmark";
static string MODE_PARAM = "MODE";
static string JSON_PARAM = "JSON_FILE_NAME";
static string DICT_PARAM = "dict";
static string INPUT_PARAM = "input";
static string AVG_LEN_PARAM = "avg_len";
static string WORD_COUNT_PARAM = "word_count";
static string SKEWNESS_PARAM = "skewness";
static string OUTPUT_PARAM = "output";

static string BST = "bst";
static string AVL = "avl";
static string RB = "rb";
static string SPLAY = "splay";
static string TRIE_ARRAY = "trieArray";
static string TRIE_LIST = "trieList";
static string TRIE_COMPACT = "trieCompact";
static string HASH_MAP = "hashMap";
static string HAT_TRIE = "hatTrie";


static string INSERT = "insert";
static string FIND = "find";
static string MIXED = "mixed";
static string REMOVE = "remove";





#endif //LICENCJAT_CONSTS_H

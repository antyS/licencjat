#include <iostream>
#include <fstream>
#include "MainUtils.h"
#include "InOutUtils.h"
#include "bench/benchUtil.h"
#include "generator/WordGenerator.h"


void MainUtils::benchmark(string inputFilename, string &outputFileName, vector<string> namesOfDictToBench) {
    chrono::time_point<system_clock, nanoseconds> start = high_resolution_clock::now(),
            readDS, readJS, bench, save;
    fstream file;
    InOutUtils::openFile(file, inputFilename, ios_base::in);
    string w;
    vector<string> dataSet;
    while (file >> w) {
        dataSet.push_back(w);
    }
    readDS = high_resolution_clock::now();
    cout << dataSet.size() << " words were read from file: " << inputFilename << " \nstart benchmarking" << endl;
    json toSave = InOutUtils::readJson(outputFileName);
    readJS = high_resolution_clock::now();
    auto results = benchDataSet(dataSet, namesOfDictToBench);
    bench = high_resolution_clock::now();
    if (toSave[BENCH] != nullptr) {
        for (auto it = results.begin(); it != results.end(); ++it) {
            if (toSave[BENCH][it.key()] == nullptr) {
                toSave[BENCH][it.key()] = it.value();
            } else {
                string error = "file: " + outputFileName
                               + " has already benchmark result for structure with name "
                               + it.key() + "\n";
                cout << error;
                throw runtime_error(error);
            }
        }
    } else {
        toSave[BENCH] = results;
    }
    InOutUtils::saveJson(toSave, outputFileName);
    save = high_resolution_clock::now();
    cout << "benchmark dataset from file" << inputFilename << " done successfully" << endl;
    cout << "benchmark results saved in file" << outputFileName << endl;
    chrono::nanoseconds allN = save - start,
            readDSN = readDS - start,
            readJSN = readJS - readDS,
            benchN = bench - readJS,
            saveN = save - bench;
    cout << "benchmark() took " << MeasureInfo::readableTime(allN) << " including:\n";
    displayMethodTime("readDS() ", readDSN, allN);
    displayMethodTime("readJson() ", readJSN, allN);
    displayMethodTime("benchDataSet() ", benchN, allN);
    displayMethodTime("saveJson() ", saveN, allN);
}

void MainUtils::displayMethodTime(const string &methodName, nanoseconds methodDur, nanoseconds allDur) {
    cout << "\t" << methodName << " -\t" << MeasureInfo::readableTime(methodDur) << " ("
         << ((double) methodDur.count() / allDur.count()) * 100
         << "%)\n";
}

void MainUtils::genAndSaveDataSet(string &filename, string &jsonStatsFilename,
                                  unsigned int avgWordLen, int wordCount, int skewness) {
    auto pathToJsonFile = InOutUtils::pathToJson(jsonStatsFilename);
    if (InOutUtils::is_file_exist(pathToJsonFile)) {
        throw runtime_error("file " + pathToJsonFile +
                            " already exists please pick another filename to avoid overriding previous results");
    }
    chrono::time_point<system_clock, chrono::nanoseconds> start = high_resolution_clock::now(), generate, stats, save;
    auto dataSet = generateDataSet(avgWordLen, wordCount, skewness / 100.0);
    generate = high_resolution_clock::now();
    json toSave;
    toSave[DATASET_STATS] = generateWordsStats(dataSet, skewness, filename, avgWordLen);
    toSave[DATASET_STATS][DATASET_ID] = Utils::generateTimeBasedId();
    stats = high_resolution_clock::now();
    InOutUtils::saveJson(toSave, jsonStatsFilename);
    save = high_resolution_clock::now();

    chrono::nanoseconds allN = save - start,
            genN = generate - start,
            statsN = stats - generate,
            saveN = save - stats;
    cout << "genAndSaveDataSet() took " << MeasureInfo::readableTime(allN) << " including:\n";
    displayMethodTime("generateDataSet()", genN, allN);
    displayMethodTime("generateWordsStats()", statsN, allN);
    displayMethodTime("saveJson()", saveN, allN);
}

json MainUtils::generateWordsStats(
        vector<string> words, int skewness, string filename, unsigned int avgWordLen) {
    json statsJson;
    map<int, map<string, int> > charCount;
    map<int, int> wordsEndCount;
    long long wordsStorage = 0;
    fstream file;
    InOutUtils::openFile(file, filename, ios_base::trunc | ios_base::out);
    for (const auto &word:words) {
        file << word << "\n";
        wordsStorage += word.size();
        wordsEndCount[word.length()] += 1;
        for (int i = 0; i < word.length(); i++) {
            charCount[i][string(1, word[i])] += 1;
        }
    }
    wordsStorage *= sizeof(words[0][0]);
    file.close();
    vector<map<string, int>> charCountVec;
    charCountVec.reserve(charCount.size());
    transform(charCount.begin(), charCount.end(), back_inserter(charCountVec),
              [](pair<const int, map<string, int> > &pair) {
                  return pair.second;
              });
    assert (charCountVec.size() == charCount.size());
    statsJson[AVG_WORD_LEN] = avgWordLen;
    statsJson[WORDS_END_COUNT] = wordsEndCount;
    statsJson[CHARS_COUNT] = charCountVec;
    statsJson[WORD_COUNT] = words.size();
    statsJson[WORDS_STORAGE_BYTES] = wordsStorage;
    statsJson[SKEWNESS] = skewness;
    return statsJson;
}

vector<string> MainUtils::generateDataSet(unsigned int avgWordLen, int wordsCount, double skewness) {
    WordGenerator wg(avgWordLen, 50, skewness);
    auto words = wg.generateSkewed(wordsCount);
    return words;
}



#ifndef LICENCJAT_UTILS_H
#define LICENCJAT_UTILS_H


#include <vector>
#include "../dict/Dict.h"

class Utils {
public:
    static unsigned int genVal();

    static std::vector<pair<string, Dict *>> generateDicts(std::vector<basic_string<char>> dictNames);

    static int generateTimeBasedId();

    static void initRand();

    static unsigned int getSeed();

private:
    static unsigned int inthash(uint64_t a, uint64_t b, uint64_t c);
};


#endif //LICENCJAT_UTILS_H

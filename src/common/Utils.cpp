#include "Utils.h"
#include <unistd.h>
#include <memory>
#include "../../test/dict/DicTestBase.h"
#include "../dict/Bst/Bst.h"
#include "../dict/Avl/Avl.h"
#include "../dict/RB/RedBlack.h"
#include "../dict/Splay/Splay.h"
#include "../dict/Trie/Trie.h"
#include "../dict/Hattrie/HashMap/HashMap.h"
#include "../dict/Hattrie/HatTrie.h"
#include "../dict/CompactTrie/CompactTrie.h"

using namespace std;


uint32_t Utils::genVal() {
    auto result = rand() + 1;
    return result;
}

vector<pair<string, Dict *>>
Utils::generateDicts(vector<string> dictNames = {HAT_TRIE, BST, AVL, RB, SPLAY, TRIE_ARRAY,TRIE_LIST, }) {
    vector<pair<string, Dict *>> dicts;
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == HAT_TRIE; })) {
        dicts.push_back({HAT_TRIE, new HatTrie()});
    }
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == BST; })) {
        dicts.push_back({BST, new Bst()});
    }
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == AVL; })) {
        dicts.push_back({AVL, new Avl()});
    }
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == RB; })) {
        dicts.push_back({RB, new RedBlack()});
    }
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == SPLAY; })) {
        dicts.push_back({SPLAY, new Splay()});
    }
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == TRIE_ARRAY; })) {
        dicts.push_back({TRIE_ARRAY, new Trie(new TrieArrayNode())});
    }
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == TRIE_LIST; })) {
        dicts.push_back({TRIE_LIST, new Trie(new TrieListNode())});
    }
    if (any_of(dictNames.begin(), dictNames.end(), [](string s) { return s == TRIE_COMPACT; })) {
        dicts.push_back({TRIE_COMPACT, new CompactTrie()});
    }
    return dicts;
}

int Utils::generateTimeBasedId() {
    Utils::initRand();
    return rand();
}

void Utils::initRand() {
    srand(getSeed());
}

unsigned int Utils::getSeed() { return inthash(clock(), time(NULL), getpid()); }

/* http://www.concentric.net/~Ttwang/tech/inthash.htm
https://gist.github.com/newpolaris/0b1aa3940efbdbb2c428f4b860e506f1#robert-jenkins-96-bit-mix-function
*/
unsigned int Utils::inthash(uint64_t a, uint64_t b, uint64_t c) {
    a = a - b;
    a = a - c;
    a = a ^ (c >> 13);
    b = b - c;
    b = b - a;
    b = b ^ (a << 8);
    c = c - a;
    c = c - b;
    c = c ^ (b >> 13);
    a = a - b;
    a = a - c;
    a = a ^ (c >> 12);
    b = b - c;
    b = b - a;
    b = b ^ (a << 16);
    c = c - a;
    c = c - b;
    c = c ^ (b >> 5);
    a = a - b;
    a = a - c;
    a = a ^ (c >> 3);
    b = b - c;
    b = b - a;
    b = b ^ (a << 10);
    c = c - a;
    c = c - b;
    c = c ^ (b >> 15);
    return c;
}

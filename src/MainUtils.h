#ifndef LICENCJAT_BENCHUTILS_H
#define LICENCJAT_BENCHUTILS_H

#include <string>
#include <vector>
#include "common/libs/json.hpp"
#include <chrono>

using namespace std;
using json = nlohmann::json;


class MainUtils {
public:
    static void benchmark(string inputFilename, string &outputFileName, vector<string> namesOfDictToBench);

    static void genAndSaveDataSet(string &filename, string &jsonStatsFilename,
                                  unsigned int avgWordLen, int wordCount, int skewness);

private:

    static vector<string> generateDataSet(unsigned int avgWordLen, int wordsCount, double skewness);

    static json generateWordsStats(vector<string> words, int skewness, string filename, unsigned int avgWordLen);

    static void displayMethodTime(const string &methodName, chrono::nanoseconds methodDur, chrono::nanoseconds allDur);

};

#endif //LICENCJAT_BENCHUTILS_H

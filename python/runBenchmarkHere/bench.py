import datetime
import json
import os
import subprocess
import sys
import time

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../plot/")

from plot.Consts import *

SKEWNESS_START = 1
SKEWNESS_STOP = 101
SKEWNESS_STEP = 5

AVG_WORD_LEN_START = 5
AVG_WORD_LEN_STOP = 41
AVG_WORD_LEN_STEP = 5

WORD_COUNT_START = 10**6
WORD_COUNT_STOP = 10**7
WORD_COUNT_STEP = 10**6

JSON_FILENAME = "jsonFileName"
DUMP_FILENAME = "saveAndStopDump.json"
FLAG_FILENAME = "saveAndStop.flag"

BENCHMARK = "benchmark"
GENERATE = "generate"
pathTobin = sys.argv[1]

# stop and save state mode
DICT_NAME = "dictName"  # as fast as possible
SKEWNESS = "skewness"  # slower than above, as fast as benched for all dictNames for constant: skewness,wordCount,avgLen
AVG_LEN = "avgLen"  # slower than above, as fast as benched for all skewnesses for constant: avgLen,wordCount,
WORD_COUNT = "wordCount"  # slower than above, as fast as benched for all avgLen for constant: wordCount
MODE = "mode"  # any of above


def log(message):
    logFile.write("%s: %s\n" % (time.strftime('%d.%m.%Y %H:%M:%S'), message))


def saveStateAndExitIfFlag(mode, variablesState):
    if os.path.isfile(FLAG_FILENAME):
        with open(FLAG_FILENAME, "r") as flag:
            fmode = flag.read()
        if fmode == mode:
            state = {MODE: mode}
            state = dict(variablesState.items() + state.items())
            with open(DUMP_FILENAME, "w+") as saveFile:
                json.dump(state, saveFile)
            log("saved state for future continuing...\nstate=%s\n" % str(state))
            os.remove(FLAG_FILENAME)
            sys.exit(14)


def continueBench():
    with open(DUMP_FILENAME, "r") as save:
        state = json.load(save)
    os.remove(DUMP_FILENAME)
    log("start continuing bench with: \nstate =%s" % str(state))
    mode = state[MODE]
    handlingNowModes = [DICT_NAME]
    if mode in handlingNowModes:
        benchmarkDicts(state[AVG_LEN], state[SKEWNESS], state[WORD_COUNT], state[JSON_FILENAME],
                       treeOrder[treeOrder.index(state[DICT_NAME]) + 1:])
        log("done continuing dictName")
    handlingNowModes.append(SKEWNESS)
    if mode in handlingNowModes:
        benchSkewness(state[SKEWNESS] + SKEWNESS_STEP, state[WORD_COUNT], state[AVG_LEN])
        log("done continuing skewness ")
    handlingNowModes.append(AVG_LEN)
    if mode in handlingNowModes:
        benchAvgWordLEn(state[AVG_LEN] + AVG_WORD_LEN_STEP, state[WORD_COUNT])
        log("done continuing avgLen ")
    handlingNowModes.append(WORD_COUNT)
    if mode in handlingNowModes:
        benchWordCount(state[WORD_COUNT] + WORD_COUNT_STEP)
        log("done continuing wordCount ")


def readableTime(elapsed):
    return "%dh:%d:min%ds:%dms" % (
        elapsed.seconds / 3600,
        (elapsed.seconds / 60) % 60,
        elapsed.seconds % 60,
        elapsed.microseconds / 1000)


def generateDataSet(wordCount, avgLen, skewness):
    start = datetime.datetime.now()
    jsonFileName = "wordsCount=%d_avgWordLen=%d_skewness=%d" % (wordCount, avgLen, skewness,)
    log("start generating %s" % jsonFileName)
    cmd = [pathTobin, GENERATE, jsonFileName, "--avg_len", str(avgLen), "--word_count", str(wordCount),
           "--skewness", str(skewness)]
    log("command to execute: " + ' '.join(cmd))
    subprocess.check_call(
        cmd, stderr=logFile, stdout=logFile)
    end = datetime.datetime.now()
    elapsed = end - start
    log("done generating %s it took %s" % (jsonFileName, readableTime(elapsed)))
    return jsonFileName


def benchmarkDicts(avgLen, skewness, wordCount, jsonFileName, dicts=treeOrder):
    for dictName in dicts:
        start = datetime.datetime.now()
        log("start benchmarking %s output in %s" % (dictName, jsonFileName))
        cmd = [pathTobin, BENCHMARK, jsonFileName, "--dict", dictName]
        log("command to execute: " + ' '.join(cmd))
        subprocess.check_call(cmd, stderr=logFile, stdout=logFile)
        end = datetime.datetime.now()
        elapsed = end - start
        log("done  benchmarking %s it took %s\noutput in %s" % (dictName, readableTime(elapsed), jsonFileName))
        saveStateAndExitIfFlag(DICT_NAME,
                               {WORD_COUNT: wordCount,
                                AVG_LEN: avgLen,
                                SKEWNESS: skewness,
                                DICT_NAME: dictName,
                                JSON_FILENAME: jsonFileName})


def benchSkewness(minSkewness, wordCount, avgLen):
    for skewness in range(minSkewness, SKEWNESS_STOP, SKEWNESS_STEP):
        jsonFileName = generateDataSet(wordCount, avgLen, skewness)
        benchmarkDicts(avgLen, skewness, wordCount, jsonFileName)
        saveStateAndExitIfFlag(SKEWNESS, {WORD_COUNT: wordCount,
                                          AVG_LEN: avgLen,
                                          SKEWNESS: skewness, })


def benchAvgWordLEn(startAvgWordLen, wordCount):
    for avgLen in range(startAvgWordLen, AVG_WORD_LEN_STOP, AVG_WORD_LEN_STEP):
        benchSkewness(SKEWNESS_START, wordCount, avgLen)
        saveStateAndExitIfFlag(AVG_LEN, {WORD_COUNT: wordCount,
                                         AVG_LEN: avgLen})


def benchWordCount(startWordCount):
    for wordCount in range(startWordCount, WORD_COUNT_STOP, WORD_COUNT_STEP):
        benchAvgWordLEn(AVG_WORD_LEN_START, wordCount)
        saveStateAndExitIfFlag(WORD_COUNT, {WORD_COUNT: wordCount})


def displayStoppingHelp():
    message = (
            'usage: ' + sys.argv[0] + ' ./pathToLicencjatBinary\n'
            + 'If you want to stop this program, create file:\n'
            + os.path.abspath(FLAG_FILENAME) + '\n' +
            'containing one of following stopping modes:\n'
            'dictName - as fast as possible\n'
            'skewness - slower than above, as fast as benched for all dictNames for constant: '
            'skewness,wordCount,avgLen\n'
            'avgLen - slower than above, as fast as benched for all skewnesses for constant: avgLen,wordCount\n'
            'wordCount - slower than above, as fast as benched for all avgLen for constant: wordCount\n'
    )
    log(message)
    print(message)


with open("log.txt", "a", 0) as logFile:
    log("___start bench.py___")
    displayStoppingHelp()
    if os.path.isfile(DUMP_FILENAME):
        continueBench()
    else:
        benchWordCount(WORD_COUNT_START)
    log("allDone")

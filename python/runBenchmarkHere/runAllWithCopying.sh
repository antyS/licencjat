#!/usr/bin/env bash
python2.7 bench.py ./licencjat
cp -R /dev/shm/bench_ka/runBenchmarkHere/benchResults /home/kadamczyk/bench/benchResults1kTo1M/
python2.7 /home/kadamczyk/licencjat/python/plot/datasetStats/__main__.py
cp -R /dev/shm/bench_ka/runBenchmarkHere/datasetStatPlots /home/kadamczyk/bench/datasetStatPlots1kTo1M/
python2.7 /home/kadamczyk/licencjat/python/plot/benchResults/__main__.py
cp -R /dev/shm/bench_ka/runBenchmarkHere/benchResultPlots /home/kadamczyk/bench/benchResultPlots1kTo1M/
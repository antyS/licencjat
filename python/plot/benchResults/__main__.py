import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../../")

from plot.Consts import *
from plot.PlotUtils import PlotUtils
from plot.benchResults.BenchResultPlot import BenchResultPlot


def main():
    print "This tool generates plots from benchmark results to help understand the impact of " \
          "benchmark parameters on performance\n" + WHERE_TO_RUN_INFO
    sys.stdout.flush()
    jsons = []
    for file in os.listdir(BENCH_RESULTS_DIR + "/"):
        if file.endswith(".json"):
            jsons.append(PlotUtils.paresJsonFile(BENCH_RESULTS_DIR + "/" + file))
    BenchResultPlot.makeAllPlots(jsons)
    print("success - end \n")


if __name__ == "__main__":
    main()

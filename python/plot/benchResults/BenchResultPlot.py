import copy
from collections import defaultdict
from operator import add

from plot.Consts import *
from plot.PlotUtils import PlotUtils
from plot.benchResults.PlotlyHelper import PlotlyHelper

NON_AVERAGABLE_RESULT_KEYS = [BENCH_ID, DATASET_ID]


class BenchResultPlot:

    def __init__(self):
        pass

    @staticmethod
    def makeAllPlots(jsonResults):
        # BenchResultPlot.exampleBarPlot()
        BenchResultPlot.makeBarPlotAveraged(jsonResults)
        # uniqueBenchmarkParameters = {}
        # benchmarkParameterKeys = [SKEWNESS, AVG_WORD_LEN, WORD_COUNT]
        # for key in benchmarkParameterKeys:
        #     uniqueBenchmarkParameters[key] = set([PlotUtils.iround(t[DATASET_STATS][key]) for t in jsonResults])
        # for key in benchmarkParameterKeys:
        #     BenchResultPlot.make3dAndTwo2dPlotsForOneChangingParam(key, uniqueBenchmarkParameters, jsonResults)
        #     BenchResultPlot.make3dPlotForTwoChangingParams(key, jsonResults, uniqueBenchmarkParameters)
        #     BenchResultPlot.makePlotsTwoAveraged(key, jsonResults, uniqueBenchmarkParameters)
        #     BenchResultPlot.makePlotsOneAveragedOneGroupedBy(key, jsonResults, uniqueBenchmarkParameters)

    @staticmethod
    def make3dPlotForTwoChangingParams(constKey, jsonResults, uniqueBenchmarkParameters):
        changingKeys = uniqueBenchmarkParameters.keys()
        changingKeys.remove(constKey)
        results = [AVG_VM_SIZE, DURATION]
        for analyzedResult in results:
            for constVal in uniqueBenchmarkParameters[constKey]:
                for groupedKeyIndex, groupedKey in enumerate(changingKeys):
                    changingKey = changingKeys[(groupedKeyIndex + 1) % len(changingKeys)]
                    filtered = filter(
                        lambda result: PlotUtils.iround(result[DATASET_STATS][constKey]) == constVal,
                        jsonResults)
                    filtered = sorted(filtered, key=lambda result: result[DATASET_STATS][changingKey])
                    benchResultsArranged = BenchResultPlot.rearrangeData(filtered)
                    filterCategories, benchResultsArranged = BenchResultPlot.addGroupedByToOperationNames(
                        benchResultsArranged, groupedKey)

                    if len(benchResultsArranged) > 0:
                        data3d, layout3d = PlotlyHelper.preparePloty3dScatterData(benchResultsArranged, analyzedResult,
                                                                                  groupedKey,
                                                                                  changingKey, jsonResults,
                                                                                  filterCategories)
                        dirname = "%sImpact/for_%s=%d-groupedBy%s" % (
                            changingKey, constKey, constVal, groupedKey)
                        PlotlyHelper.make_plot(data3d, layout3d, dirname,
                                               "on_" + resultCategories[analyzedResult][TITLE])

    @staticmethod
    def addGroupedByToOperationNames(benchResultsArranged, groupedKey):
        groupedCategories = set()
        for treeName, treeResults in benchResultsArranged.items():
            toAverage = defaultdict(  # groupSuffix
                lambda: defaultdict(  # resultKey
                    list))
            for operationName, operationResult in treeResults.items():
                uniqueGroupedVals = set(
                    map(lambda x: PlotUtils.iround(x), operationResult[groupedKey]))
                groupedCategories |= uniqueGroupedVals
                for groupVal in uniqueGroupedVals:
                    groupIds = [index for index, gv in
                                enumerate(operationResult[groupedKey])
                                if PlotUtils.iround(gv) == groupVal]
                    groupLabelSuffix = "-" + groupedKey + "=" + str(groupVal)
                    for resultKey in operationResult.iterkeys():
                        suffixedOperationName = operationName + groupLabelSuffix
                        treeResults[suffixedOperationName][resultKey] = \
                            [val for idx, val in
                             enumerate(treeResults[operationName][resultKey])
                             if idx in groupIds]
                        toAverage[groupLabelSuffix][resultKey] \
                            .append(treeResults[suffixedOperationName][resultKey])
                benchResultsArranged[treeName].pop(operationName)

            for groupSuffix, groupResults in toAverage.iteritems():
                for resultKey, results in groupResults.iteritems():
                    if (resultKey in NON_AVERAGABLE_RESULT_KEYS):
                        groupResults[resultKey] = results[0]
                    else:
                        groupItemCount = len(results)
                        groupResults[resultKey] = reduce(lambda x, y: map(add, x, y), results)
                        groupResults[resultKey] = map(lambda x: x / float(groupItemCount),
                                                      groupResults[resultKey])
                benchResultsArranged[treeName][FAKE_OPERATION_AVERAGE + groupSuffix] = groupResults
        filterCategories = sorted(list(groupedCategories))
        filterCategories = [groupedKey + "=" + str(cat) for cat in filterCategories]
        return filterCategories, benchResultsArranged

    @staticmethod
    def make3dAndTwo2dPlotsForOneChangingParam(key, uniqueBenchmarkParameters, jsonResults):
        constKeys = uniqueBenchmarkParameters.keys()
        constKeys.remove(key)
        for param0 in uniqueBenchmarkParameters[constKeys[0]]:
            for param1 in uniqueBenchmarkParameters[constKeys[1]]:
                filtered = filter(
                    lambda result: PlotUtils.iround(
                        result[DATASET_STATS][constKeys[0]]) == param0 and PlotUtils.iround(
                        result[DATASET_STATS][constKeys[1]]) == param1,
                    jsonResults)
                filtered = sorted(filtered, key=lambda result: result[DATASET_STATS][key])
                arranged = BenchResultPlot.rearrangeData(filtered)
                arranged = BenchResultPlot.addFakeAverageOperation(arranged)

                if len(arranged) > 0:
                    data3d, layout3d = PlotlyHelper.preparePloty3dScatterData(arranged, AVG_VM_SIZE, DURATION, key,
                                                                              jsonResults)
                    dirname = "%sImpact/for_%s=%d-%s=%d" % (key, constKeys[0], param0, constKeys[1], param1)
                    PlotlyHelper.make_plot(data3d, layout3d, dirname,
                                           "on_durationAndMemory")
                    BenchResultPlot.make_plot2d(data3d, dirname, layout3d, DURATION_LABEL)
                    BenchResultPlot.make_plot2d(data3d, dirname, layout3d, MEMORY_LABEL)

    @staticmethod
    def addFakeAverageOperation(arranged):
        for treename, treeResult in arranged.iteritems():
            treeResultItems = treeResult.items()
            average = copy.deepcopy(treeResultItems[0][1])
            for itemIndex in range(1, len(treeResultItems)):
                for resultKey in average.iterkeys():
                    if resultKey not in NON_AVERAGABLE_RESULT_KEYS:
                        for j in range(len(average[resultKey])):
                            average[resultKey][j] += treeResultItems[itemIndex][1][resultKey][j]
            for resultKey in average.iterkeys():
                if resultKey not in NON_AVERAGABLE_RESULT_KEYS:
                    for j in range(len(average[resultKey])):
                        average[resultKey][j] /= float(len(treeResultItems))
            arranged[treename][FAKE_OPERATION_AVERAGE] = average
        return arranged

    @staticmethod
    def makePlotsTwoAveraged(key, jsonResults, uniqueBenchmarkParameters):
        averagedKeys = uniqueBenchmarkParameters.keys()
        averagedKeys.remove(key)

        sort = sorted(jsonResults, key=lambda result: result[DATASET_STATS][key])
        averaged = BenchResultPlot.rearrangeData(sort)
        for treeName, treeResults in averaged.items():
            for operationName, operationResult in treeResults.items():
                for resultKey, resultArray in operationResult.items():
                    averaged[treeName][operationName][resultKey] = []
        for unique in sorted(uniqueBenchmarkParameters[key]):
            for treeName, treeResults in averaged.items():
                for operationName, operationResult in treeResults.items():
                    averaged[treeName][operationName][key].append(unique)
                    averaged[treeName][operationName][BENCH_ID].append("n/a")
                    averaged[treeName][operationName][DATASET_ID].append("n/a")

                    filtered = filter(
                        lambda result: PlotUtils.iround(result[DATASET_STATS][key]) == unique,
                        jsonResults)
                    arranged = BenchResultPlot.rearrangeData(filtered)

                    for resultKey, resultArray in operationResult.items():
                        if resultKey not in [key, BENCH_ID, DATASET_ID]:
                            averaged[treeName][operationName][resultKey].append(
                                sum(arranged[treeName][operationName][resultKey])
                                / float(len(arranged[treeName][operationName][resultKey]))
                            )
        averaged = BenchResultPlot.addFakeAverageOperation(averaged)
        if len(averaged) > 0:
            data3d, layout3d = PlotlyHelper.preparePloty3dScatterData(averaged, AVG_VM_SIZE, DURATION, key, jsonResults)
            dirname = "%sImpact/for_averaged-%sAndAveraged-%s" % (key, averagedKeys[0], averagedKeys[1])
            PlotlyHelper.make_plot(data3d, layout3d, dirname,
                                   "on_durationAndMemory")
            BenchResultPlot.make_plot2d(data3d, dirname, layout3d, DURATION_LABEL)
            BenchResultPlot.make_plot2d(data3d, dirname, layout3d, MEMORY_LABEL)

    @staticmethod
    def makePlotsOneAveragedOneGroupedBy(key, jsonResults, uniqueBenchmarkParameters):
        otherKeys = uniqueBenchmarkParameters.keys()
        otherKeys.remove(key)

        sort = sorted(jsonResults, key=lambda result: result[DATASET_STATS][key])
        for groupedKey in otherKeys:
            averaged = BenchResultPlot.rearrangeData(sort)
            for treeName, treeResults in averaged.items():
                for operationName, operationResult in treeResults.items():
                    for resultKey, resultArray in operationResult.items():
                        averaged[treeName][operationName][resultKey] = []

            for impactVal in sorted(uniqueBenchmarkParameters[key]):
                for groupedVal in uniqueBenchmarkParameters[groupedKey]:
                    for treeName, treeResults in averaged.items():
                        for operationName, operationResult in treeResults.items():
                            averaged[treeName][operationName][key].append(impactVal)
                            averaged[treeName][operationName][BENCH_ID].append("n/a")
                            averaged[treeName][operationName][DATASET_ID].append("n/a")

                            filtered = filter(
                                lambda result: PlotUtils.iround(result[DATASET_STATS][key]) == impactVal and
                                               PlotUtils.iround(result[DATASET_STATS][groupedKey]) == groupedVal,
                                jsonResults)
                            arranged = BenchResultPlot.rearrangeData(filtered)

                            for resultKey, resultArray in operationResult.items():
                                if resultKey not in [key, BENCH_ID, DATASET_ID]:
                                    averaged[treeName][operationName][resultKey].append(
                                        sum(arranged[treeName][operationName][resultKey])
                                        / float(len(arranged[treeName][operationName][resultKey]))
                                    )
            averaged = BenchResultPlot.addFakeAverageOperation(averaged)
            filterCategories, averaged = BenchResultPlot.addGroupedByToOperationNames(
                averaged, groupedKey)
            if len(averaged) > 0:
                data3d, layout3d = PlotlyHelper.preparePloty3dScatterData(averaged, AVG_VM_SIZE, DURATION, key,
                                                                          jsonResults,
                                                                          filterCategories)
                dirname = "%sImpact/for_averaged-%s-groupedBy%s" % (
                    key, filter(lambda x: x is not groupedKey, otherKeys)[0], groupedKey)
                PlotlyHelper.make_plot(data3d, layout3d, dirname,
                                       "on_durationAndMemory")
                BenchResultPlot.make_plot2d(data3d, dirname, layout3d, DURATION_LABEL)
                BenchResultPlot.make_plot2d(data3d, dirname, layout3d, MEMORY_LABEL)

    @staticmethod
    def make_plot2d(data3d, dirname, layout3d, yaxisLabel):
        data2d = copy.deepcopy(data3d)
        axisKeyForYaxisLabel = "y" if layout3d[SCENE]["yaxis"]["title"] == yaxisLabel \
            else "x"
        for d2d in data2d:
            d2d["type"] = "scatter"
            d2d["y"] = d2d[axisKeyForYaxisLabel]
            d2d["x"] = d2d[Z]
            d2d["marker"]["size"] = 3
            del d2d[Z]
        layout2d = copy.deepcopy(layout3d)
        layout2d["yaxis"] = layout2d[SCENE][axisKeyForYaxisLabel + "axis"]
        layout2d["xaxis"] = layout2d[SCENE]["zaxis"]
        del layout2d[SCENE]
        PlotlyHelper.make_plot(data2d, layout2d, dirname,
                               "on_" + yaxisLabel)

    @staticmethod
    def rearrangeData(jsonResults):
        arranged = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        for benchResult in jsonResults:
            for treeName, tree in benchResult[BENCH].iteritems():
                for operationName, treeResult in tree.iteritems():
                    arranged[treeName][operationName][AVG_VM_SIZE].append(
                        PlotlyHelper.kBtoB(treeResult[MEMORY_INFO][AVG_VM_SIZE]))
                    arranged[treeName][operationName][DURATION].append(
                        PlotlyHelper.nanosToSec(treeResult[DURATION]))
                    arranged[treeName][operationName][BENCH_ID].append(
                        treeResult[BENCH_ID])

                    arranged[treeName][operationName][SKEWNESS].append(
                        benchResult[DATASET_STATS][SKEWNESS])
                    arranged[treeName][operationName][WORD_COUNT].append(
                        benchResult[DATASET_STATS][WORD_COUNT])
                    arranged[treeName][operationName][AVG_WORD_LEN].append(
                        benchResult[DATASET_STATS][AVG_WORD_LEN])
                    arranged[treeName][operationName][WORDS_STORAGE_BYTES].append(
                        benchResult[DATASET_STATS][WORDS_STORAGE_BYTES])

                    arranged[treeName][operationName][DATASET_ID].append(
                        benchResult[DATASET_STATS][DATASET_ID])
        return arranged

    @staticmethod
    def makeBarPlotAveraged(jsonResults):
        arranged = BenchResultPlot.rearrangeData(jsonResults)
        averaged = copy.deepcopy(arranged)
        for treeName, treeResults in averaged.items():
            for operationName, operationResult in treeResults.items():
                for resultKey, resultArray in operationResult.items():
                    averaged[treeName][operationName][resultKey] = []
        for treeName, treeResults in averaged.items():
            for operationName, operationResult in treeResults.items():
                averaged[treeName][operationName][BENCH_ID].append("n/a")
                averaged[treeName][operationName][DATASET_ID].append("n/a")

                for resultKey, resultArray in operationResult.items():
                    if resultKey not in [BENCH_ID, DATASET_ID]:
                        averaged[treeName][operationName][resultKey].append(
                            sum(arranged[treeName][operationName][resultKey])
                            / float(len(arranged[treeName][operationName][resultKey]))
                        )
        averaged = BenchResultPlot.addFakeAverageOperation(averaged)
        if len(averaged) > 0:
            for resultKey in [DURATION, AVG_VM_SIZE]:
                data, layout = PlotlyHelper.prepareBarPlotData(averaged, resultKey)
                PlotlyHelper.make_plot(data, layout, "",
                                       "allAverage_" + resultKey)

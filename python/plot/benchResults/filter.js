function ready(fn) {
    if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(function () {
    if (!Array.prototype.last) {
        Array.prototype.last = function () {
            return this[this.length - 1];
        };
    }
    var plotlyDivID = '40369bd1-f21b-4764-99a7-24884bbc5dd2';
    var plotlyDiv = document.getElementById(plotlyDivID);
    plotlyDiv.filter = {};
    plotlyDiv.on('plotly_buttonclicked', function (event) {
            if (event.button.execute) {
                return;
            }
            var label = event.button.label;
            var category = event.button.args[0].categoryName;
            if (label.startsWith('All')) {
                $.each(plotlyDiv.filter, function (key, val) {
                    delete plotlyDiv.filter[key];
                })
            } else {
                if (!(category in plotlyDiv.filter)) {
                    plotlyDiv.filter[category] = {}
                }
                if (label in plotlyDiv.filter[category]) {
                    delete plotlyDiv.filter[category][label];
                    if (Object.keys(plotlyDiv.filter[category]).length <= 0) {
                        delete plotlyDiv.filter[category]
                    }
                } else {
                    plotlyDiv.filter[category][label] = event.button.args[0].visible
                }
            }
            var result = Array(plotlyDiv._fullData.length).fill(true);
            var toastText = '';
            $.each(plotlyDiv.filter, function (catK, catV) {
                toastText += "<br>" + catK + ":<br>\t";
                var catResult = Array(plotlyDiv._fullData.length).fill(false);
                $.each(catV, function (labK, labV) {
                    $.each(labV, function (i) {
                        catResult[i] = catResult[i] || labV[i];
                    });
                    toastText += labK.split("=").last() + " or "
                });
                $.each(result, function (i) {
                    result[i] = result[i] && catResult[i];
                });
                toastText += "<br>"
            });
            toastText = toastText.replace(/ or <br>/g, '');
            if (label.startsWith("All")) {
                toastText = 'Filter reset, showing all.'
            }
            $.toast({heading: 'Filter', text: toastText, stack: false, hideAfter: 5000,position: 'top-right'});
            Plotly.Plots.executeAPICommand(plotlyDiv, event.button.method, [{visible: result}]);
        }
    );
});
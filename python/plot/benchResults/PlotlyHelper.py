import os
import re
import sys
import time

import plotly
import plotly.graph_objs as go

from plot.Consts import *
from plot.PlotUtils import PlotUtils

testIntegrity = False


class PlotlyHelper:
    def __init__(self):
        pass

    @staticmethod
    def preparePloty3dScatterData(benchResults, onXAxis, onYAxis, onZAxis, jsonResults, filterCategories=[]):
        operationResultInOrder = []
        data = []

        benchResultsItems = benchResults.items()
        benchResultsItems.sort(key=lambda x: treeOrder.index(x[0]))
        for treeName, treeResult in benchResultsItems:
            treeResultItems = treeResult.items()
            treeResultItems.sort(cmp=PlotlyHelper.compareTreeResultItems)
            for operationName, operationResult in treeResultItems:
                operationResultInOrder.append(operationResult)
                data.append(go.Scatter3d(
                    x=operationResult[onXAxis],
                    y=operationResult[onYAxis],
                    z=operationResult[onZAxis],
                    mode='lines+markers',
                    marker=go.Marker(
                        size=1.1,
                        color=colorsSimilarStructures[len(data) % len(colorsSimilarStructures)]
                    ),
                    line=go.Line(
                        width=1
                    ),
                    name="%s-%s" % (treeName, operationName),
                    hoverinfo="text",
                    text=["%s-%s<br>"
                          "---bench---<br>"
                          "%s:\t%s<br>"
                          "%s:\t%s<br>"
                          "---dataset---<br>"
                          "skewness:\t%s%%<br>"
                          "words count:\t%d<br>"
                          "average word length:\t%s<br>"
                          "words storage:\t%s<br>"
                          "benchId:\t%s<br>"
                          "dataSetId :\t%s<br>"
                          % (
                              treeName, operationName,
                              DURATION_LABEL, PlotUtils.readableDuration(sec),
                              MEMORY_LABEL, PlotUtils.readableMemory(memB),
                              ("%.1f" % skew).rstrip('0').rstrip('.'),
                              wordCount,
                              ("%.1f" % avgWordLen).rstrip('0').rstrip('.'),
                              PlotUtils.readableMemory(stoB),
                              str(benchId),
                              str(dataSetId)
                          )
                          for sec, memB, stoB, skew, avgWordLen, wordCount, benchId, dataSetId in
                          zip(operationResult[DURATION],
                              operationResult[AVG_VM_SIZE],
                              operationResult[WORDS_STORAGE_BYTES],
                              operationResult[SKEWNESS],
                              operationResult[AVG_WORD_LEN],
                              operationResult[WORD_COUNT],
                              operationResult[BENCH_ID],
                              operationResult[DATASET_ID],
                              )]
                ))
                if testIntegrity:
                    PlotlyHelper.testNotMessedUp(jsonResults, operationResult, treeName, operationName)

        updatemenus = list([
            dict(active=0,
                 xanchor='left',
                 yanchor='top',
                 buttons=list(
                     [PlotlyHelper.makeFilterButtonGeneric("All", [True * len(data)], "reset")]
                     + PlotlyHelper.makeFilterButtons(data, operationOrder, "operation")
                     + PlotlyHelper.makeFilterButtons(data, treeOrder, "tree")
                     + PlotlyHelper.makeFilterButtons(data, filterCategories, resultCategories[onYAxis][TITLE])
                 ),
                 showactive=False
                 ),
            dict(active=0,
                 xanchor='left',
                 yanchor='top',
                 y=0.92,
                 buttons=[
                     PlotlyHelper.makeColorButton("similar colors",
                                                  PlotUtils.circleColors(colorsSimilarStructures, len(data))),
                     PlotlyHelper.makeColorButton("random colors",
                                                  PlotUtils.circleColors(colorsRandom, len(data))),
                 ]
                 ),
        ])
        layout = go.Layout(
            hovermode="closest",
            legend=go.Legend(y=0.5),
            margin=go.Margin(
                l=0,
                r=0,
                b=30,
                t=0,
            ),
            title="",
            updatemenus=updatemenus,
            showlegend=True,
            scene=go.Scene(
                aspectmode="cube",
                dragmode="orbit",
                xaxis=go.XAxis(title=resultCategories[onXAxis][TITLE],
                               range=PlotlyHelper.minMaxRange(data, "x"),
                               tickformat=".3s",
                               ticksuffix=resultCategories[onXAxis][UNIT]),
                yaxis=go.YAxis(title=resultCategories[onYAxis][TITLE],
                               range=PlotlyHelper.minMaxRange(data, "y"),
                               tickformat=".3s",
                               ticksuffix=resultCategories[onYAxis][UNIT],
                               ),
                zaxis=go.ZAxis(title=resultCategories[onZAxis][TITLE],
                               range=PlotlyHelper.minMaxRange(data, "z"),
                               tickformat=".3s",
                               ticksuffix=resultCategories[onZAxis][UNIT]),
                camera=dict(
                    center=dict(x=0, y=0, z=0),
                    eye=dict(x=-1.555, y=0.068, z=-1.303),
                    up=dict(x=0.034, y=0.999, z=0.012),
                ),
            )
        )
        return data, layout

    @staticmethod
    def make_plot(data, layout, dirname, filename):
        fig = go.Figure(data=data, layout=layout)
        plotDiv = plotly.offline.plot(fig, validate=False,
                                      output_type="div",
                                      include_plotlyjs=False,
                                      show_link=False)
        divId = PlotUtils.getPlotDivId(plotDiv)
        filterScript = PlotlyHelper.getFilterScript()
        htmlContent = htmlTemplate.format(plotContentDiv=plotDiv, resizeScript=resizeScript.format(id=divId),
                                          filterScript=filterScript.replace(UNIQUE_TOKEN_DIV_ID, divId))
        filePath = "%s/%s/" % (BENCH_PLOTS_OUTPUT_DIR, dirname)
        PlotUtils.assure_path_exists(filePath)
        filePath += "%s__plotGeneratedOn=%d.html" % (filename, time.time())
        with open(filePath, 'w') as f:
            f.write(htmlContent)
            print("file %s was created\n" % filePath)

    @staticmethod
    def getFilterScript():
        with open(PlotlyHelper.get_script_path() + FILTER_JS, "r")as f:
            filterScript = f.read()
        filterScript = re.sub("var plotlyDivID = '\S+", "var plotlyDivID = '%s';" % UNIQUE_TOKEN_DIV_ID, filterScript)
        return filterScript

    @staticmethod
    def compareTreeResultItems(x, y):
        xo = PlotlyHelper.getOrder(x, operationOrder)
        yo = PlotlyHelper.getOrder(y, operationOrder)
        if xo != yo:
            return xo - yo
        else:
            xSuf = int(x[0].split("=")[1])
            ySuf = int(y[0].split("=")[1])
            return xSuf - ySuf

    @staticmethod
    def getOrder(item, order):
        return (oper[0] for oper in enumerate(order) if item[0].startswith(oper[1])).next()

    @staticmethod
    def makeFilterButtons(data, filterCategories, categoryName):
        buttons = []
        for category in filterCategories:
            buttons.append(PlotlyHelper.makeFilterButton(category, data, categoryName))
        return buttons

    @staticmethod
    def makeFilterButton(inName, data, categoryName):
        if '=' not in inName:
            def func(part, label):
                return part in label
        else:
            def func(part, label):
                return label.endswith(str(part))

        return PlotlyHelper.makeFilterButtonGeneric(inName,
                                                    [True if func(inName, plot[NAME]) else False for plot in data],
                                                    categoryName)

    @staticmethod
    def makeFilterButtonGeneric(label, visibleBoolArray, categoryName):
        return PlotlyHelper.makeButton(label, [{'visible': visibleBoolArray, "categoryName": categoryName}],
                                       execute=False)

    @staticmethod
    def makeColorButton(name, colors):
        return PlotlyHelper.makeButton(name, [{'marker.color': colors},
                                              ], )

    @staticmethod
    def makeButton(label, args, method='restyle', execute=True):
        return dict(label=label,
                    method=method,
                    args=args,
                    execute=execute)

    @staticmethod
    def minMaxRange(data, axis):
        return [min([min(plot[axis]) for plot in data]),
                max([max(plot[axis]) for plot in data])]

    @staticmethod
    def get_script_path():
        return os.path.dirname(os.path.realpath(sys.argv[0]))

    @staticmethod
    def testNotMessedUp(jsonResults, operationResult, treeName, operationName):
        if operationName is FAKE_OPERATION_AVERAGE or "=" in operationName:
            return
        for i in range(0, len(operationResult[DATASET_ID])):
            filtered = filter(lambda j: j[DATASET_STATS][DATASET_ID] == operationResult[DATASET_ID][i], jsonResults)
            assert len(filtered) == 1, "Only one dataset with id %s should exist but %d datasets exists" % (
                operationResult[DATASET_ID][i], len(filtered))
            filtered = filtered[0]
            msg = "data in json do not match data on plot"
            assert filtered[BENCH][treeName][operationName][BENCH_ID] == operationResult[BENCH_ID][i], msg
            assert PlotlyHelper.nanosToSec(filtered[BENCH][treeName][operationName][DURATION]) == \
                   operationResult[DURATION][i], msg
            assert PlotlyHelper.kBtoB(
                filtered[BENCH][treeName][operationName][MEMORY_INFO][AVG_VM_SIZE]) == operationResult[AVG_VM_SIZE][
                       i], msg
            assert filtered[DATASET_STATS][WORDS_STORAGE_BYTES] == operationResult[WORDS_STORAGE_BYTES][i], msg
            assert filtered[DATASET_STATS][SKEWNESS] == operationResult[SKEWNESS][i], msg
            assert filtered[DATASET_STATS][AVG_WORD_LEN] == operationResult[AVG_WORD_LEN][i], msg
            assert filtered[DATASET_STATS][WORD_COUNT] == operationResult[WORD_COUNT][i], msg
            assert filtered[DATASET_STATS][DATASET_ID] == operationResult[DATASET_ID][i], msg

    @staticmethod
    def kBtoB(kB):
        return kB * 1024

    @staticmethod
    def nanosToSec(nanos):
        return nanos / (10.0 ** 9)

    @staticmethod
    def prepareBarPlotData(benchResults, onYAxis):
        # arranged[treeName][operationName][SKEWNESS]
        data = []
        treeNames = benchResults.keys()
        for operationName in operationOrder:
            yVals = [treeResult[operationName][onYAxis][0] for treeResult in benchResults.values()]
            data.append(go.Bar(
                x=treeNames,
                y=yVals,
                hoverinfo="text",
                hovertext=PlotlyHelper.hoverTextBar(benchResults, operationName, treeNames),
                text=[round(val, 1) for val in yVals],
                textposition='auto',
                name=operationName,
            ))
        buttons = []
        for operation in operationOrder:
            buttons.append(PlotlyHelper.makeButton("Sort by " + operation, [
                {'xaxis.categoryarray': PlotlyHelper.categoryArrayForBar(benchResults, onYAxis, operation)}],
                                                   method="relayout"))
        updatemenus = list([
            dict(active=4,
                 xanchor='left',
                 yanchor='top',
                 buttons=buttons
                 ),
        ])
        layout = go.Layout(
            updatemenus=updatemenus,
            legend=go.Legend(y=0.5),
            margin=go.Margin(
                l=0,
                r=0,
                b=30,
                t=0,
            ),
            xaxis=go.XAxis(
                categoryorder='array',
                categoryarray=PlotlyHelper.categoryArrayForBar(benchResults, onYAxis,
                                                               FAKE_OPERATION_AVERAGE)),
            barmode='group',
            yaxis=go.YAxis(title=resultCategories[onYAxis][TITLE],
                           tickformat=".3s",
                           ticksuffix=resultCategories[onYAxis][UNIT],
                           ),

        )
        return data, layout

    @staticmethod
    def hoverTextBar(benchResults, operationName, treeNames):
        result = []
        for treeName in treeNames:
            operationResults = benchResults[treeName][operationName]
            result.append("---bench---<br>"
                          "%s:\t%s<br>"
                          "%s:\t%s<br>"
                          "---dataset---<br>"
                          "skewness:\t%s%%<br>"
                          "words count:\t%d<br>"
                          "average word length:\t%s<br>"
                          "words storage:\t%s<br>"
                          % (
                              DURATION_LABEL, PlotUtils.readableDuration(operationResults[DURATION][0]),
                              MEMORY_LABEL, PlotUtils.readableMemory(operationResults[AVG_VM_SIZE][0]),
                              ("%.1f" % operationResults[SKEWNESS][0]).rstrip('0').rstrip('.'),
                              operationResults[WORD_COUNT][0],
                              ("%.1f" % operationResults[AVG_WORD_LEN][0]).rstrip('0').rstrip('.'),
                              PlotUtils.readableMemory(operationResults[WORDS_STORAGE_BYTES][0])
                          ))
        return result

    @staticmethod
    def categoryArrayForBar(benchResults, onYAxis, operation):
        catArr = [(treeName, benchResults[treeName][operation][onYAxis][0]) for treeName in
                  benchResults.keys()]
        catArr = sorted(catArr, key=lambda x: x[1])
        catArr = [item[0] for item in catArr]
        return catArr

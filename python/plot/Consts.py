UNIT = "unit"
Z = "z"

SCENE = "scene"

BST = "bst"
AVL = "avl"
RB = "rb"
SPLAY = "splay"
TRIE_ARRAY = "trieArray"
TRIE_COMPACT = "trieCompact"
TRIE_LIST = "trieList"
HAT_TRIE = "hatTrie"
MIXED = 'mixed'
INSERT = 'insert'
FIND = 'find'
REMOVE = 'remove'
# average from all operations for given tree
FAKE_OPERATION_AVERAGE = 'average'
NAME = "name"

TITLE = "title"
BENCH_PLOTS_OUTPUT_DIR = "benchResultPlots"
STAT_PLOTS_OUTPUT_DIR = "datasetStatPlots"
BENCH_RESULTS_DIR = "benchResults"
WORD_COUNT = "wordCount"
CHARS_COUNT = "charCount"
WORDS_END_COUNT = "wordsEndCount"
AVG_WORD_LEN = "avgWordLen"
WORDS_STORAGE_BYTES = "wordsStorageBytes"
DATASET_STATS = "datasetStats"
BENCH = "bench"
SKEWNESS = "skewness"
AVG_VM_SIZE = "avgVmSize"
DURATION = "duration-ns"
MEMORY_INFO = "memoryInfo-kB"
BENCH_ID = "benchId"
DATASET_ID = "datasetId"

DURATION_LABEL = "duration"
MEMORY_LABEL = "memory"
resultCategories = {
    AVG_VM_SIZE: {TITLE: MEMORY_LABEL, UNIT: "B"},
    DURATION: {TITLE: DURATION_LABEL, UNIT: "s"},
    SKEWNESS: {TITLE: SKEWNESS, UNIT: "%"},
    WORD_COUNT: {TITLE: WORD_COUNT, UNIT: ""},
    AVG_WORD_LEN: {TITLE: AVG_WORD_LEN, UNIT: ""},
}

treeOrder = [BST, SPLAY, AVL, RB, TRIE_LIST, TRIE_ARRAY, TRIE_COMPACT, HAT_TRIE]
operationOrder = [INSERT, FIND, MIXED, REMOVE, FAKE_OPERATION_AVERAGE]

FILTER_JS = "/filter.js"

resizeScript = (
    ''
    '<script type="text/javascript">'
    'window.addEventListener("resize", function(){{'
    'Plotly.Plots.resize(document.getElementById("{id}"));}});'
    '</script>'
)
colorsSimilarStructures = ["#9a4862", "#db3a60", "#dc7e7f", "#c54949", "#cd4a2b", "#e59169", "#df792c", "#9a5c30",
                           "#c48a2f", "#daae34", "#c2aa68", "#7d742d", "#a6b642", "#567e2f", "#5bba4d", "#529b66",
                           "#51c2a6", "#4db9df", "#6092d3", "#5263a7", "#5e77e5", "#6d50b0", "#b293d9", "#9964dc",
                           "#da81d8", "#bc49bc", "#985196", "#e24897", "#de83ab", "#b33974"]
colorsRandom = ["#9964dc", "#7d742d", "#b293d9", "#51c2a6", "#4db9df", "#c54949", "#9a4862", "#bc49bc", "#a6b642",
                "#5bba4d", "#cd4a2b", "#529b66", "#df792c", "#de83ab", "#daae34", "#c48a2f", "#9a5c30", "#dc7e7f",
                "#b33974", "#e24897", "#e59169", "#985196", "#6092d3", "#6d50b0", "#da81d8", "#c2aa68", "#567e2f",
                "#db3a60", "#5e77e5", "#5263a7"]

htmlTemplate = '<html>' \
               '<head>' \
               '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css">' \
               '<script src="https://cdn.plot.ly/plotly-1.36.0.min.js"></script>' \
               '<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>' \
               '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>' \
               '<script >{filterScript}</script>' \
               '</head>' \
               '<body>' \
               '{plotContentDiv}' \
               '{resizeScript}' \
               '</body>' \
               '</html>'
UNIQUE_TOKEN_DIV_ID = "uniqueToken-plodDivID"
WHERE_TO_RUN_INFO = "please run it in directory with benchResults folder " \
                    "with json files containing result of benchmark and datasetStats"

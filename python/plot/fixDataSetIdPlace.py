import json
import os

from Consts import *
from PlotUtils import PlotUtils

fixedCount = 0
for file in os.listdir("."):
    if file.endswith(".json"):
        result = PlotUtils.paresJsonFile(file)
        if DATASET_ID in result:
            result[DATASET_STATS][DATASET_ID] = result[DATASET_ID]
            del result[DATASET_ID]
            with open(file, "w") as f:
                json.dump(result, f)
            print file + " was fixed\n"
            fixedCount += 1
print "successfully fixed %d files - end\n" % fixedCount

import json
import os

from Consts import *
from PlotUtils import PlotUtils

fixedCount = 0
for file in os.listdir("."):
    if file.endswith(".json"):
        result = PlotUtils.paresJsonFile(file)
        if TRIE_ARRAY in result[BENCH] or TRIE_LIST in result[BENCH]:
            del result[BENCH][TRIE_LIST]
            del result[BENCH][TRIE_ARRAY]
            with open(file, "w") as f:
                json.dump(result, f)
            print file + " was fixed\n"
            fixedCount += 1
print "successfully fixed %d files - end\n" % fixedCount

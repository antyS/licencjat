import json
import os
import re


class PlotUtils:

    def __init__(self):
        pass

    @staticmethod
    def iround(x):
        """iround(number) -> integer
        Round a number to the nearest integer."""
        y = round(x) - .5
        return int(y) + (y > 0)

    @staticmethod
    def readableDuration(sec):
        array = [(sec / 3600, "h"),
                 ((sec / 60) % 60, "min"),
                 (sec % 60, "s"),
                 ((sec * 1e3) % 1e3, "ms"),
                 ((sec * 1e6) % 1e3, "us")
                 ]
        return PlotUtils.readableGeneric(array)

    @staticmethod
    def readableMemory(memB):
        array = [((memB / (1e3 ** 3)), "GB"),
                 ((memB / (1e3 ** 2)) % (1e3 ** 2), "MB"),
                 ((memB / 1e3) % 1e3, "kB"),
                 (memB % 1e3, "B")
                 ]
        return PlotUtils.readableGeneric(array)

    @staticmethod
    def readableGeneric(array):
        result = ""
        started = False
        for (val, unit) in array:
            if round(val) > 0 or started is True:
                started = True
                result += "%d%s " % (val, unit)
        return result

    @staticmethod
    def assure_path_exists(path):
        dir = os.path.dirname(path)
        if not os.path.exists(dir):
            os.makedirs(dir)

    @staticmethod
    def circleColors(colors, length):
        return [colors[i % len(colors)] for i in
                range(length)]

    @staticmethod
    def getPlotDivId(plotDiv):
        divIdSearch = re.search('<div id=\"([\w,\-]+)\"', plotDiv, re.IGNORECASE)
        if divIdSearch:
            divId = divIdSearch.group(1)
        else:
            raise Exception("error extracting plot div id")
        return divId

    @staticmethod
    def paresJsonFile(filePath):
        with open(filePath, 'r') as read_file:
            parsed = json.load(read_file)
        return parsed

from collections import defaultdict

import plotly
import plotly.graph_objs as go

from plot.Consts import *
from plot.PlotUtils import PlotUtils

htmlTemplate = '<html>' \
               '<head>' \
               '<script src="https://cdn.plot.ly/plotly-1.36.0.min.js"></script>' \
               '</head>' \
               '<body>' \
               '{plotContentDiv}' \
               '{resizeScript}' \
               '</body>' \
               '</html>'


class ProbabilityPlot:

    def __init__(self):
        pass

    @staticmethod
    def makePlot(wordsStats, datasetName):
        wordsStats = wordsStats[DATASET_STATS]
        data = []
        charCount = wordsStats[CHARS_COUNT]
        wordsEndProbs = defaultdict(int, {key: value for (key, value) in wordsStats[WORDS_END_COUNT]})
        wordsCount = wordsStats[WORD_COUNT]

        for index in wordsEndProbs.iterkeys():
            wordsEndProbs[index] /= float(wordsCount)
            wordsEndProbs[index] *= 100

        wordsEndProbs = [wordsEndProbs[i] for i in range(1, len(charCount) + 1)]

        for index, charsProb in enumerate(charCount):
            charsCount = sum(charsProb.values())
            for char in charsProb:
                charsProb[char] /= float(charsCount)
            charsProb = charsProb.iteritems()
            charsProb = sorted(charsProb, key=lambda t: t[1], reverse=True)
            label = str(index + 1) + ": " + ''.join(str(x[0]) for x in charsProb)
            probabilities = [t[1] * 100 for t in charsProb]
            hoverText = ["char: %s<br>position: %d<br>probability: %.2f%%" % (t[0], index + 1, t[1] * 100) for t in
                         charsProb]
            data.append(go.Scatter3d(
                x=range(len(charsProb)),  # chars
                y=probabilities,
                z=[index + 1] * (len(charsProb)),
                marker=go.Marker(
                    size=1,
                ),
                line=go.Line(
                    width=1
                ),
                name=label,
                hoverinfo="text",
                text=hoverText
            ))
        allWordsLengths = range(1, len(charCount) + 1)
        maxXaxis = max([len(charsProb) for charsProb in charCount]) + 2
        lineAfterOthers = [maxXaxis] * len(charCount)
        assert (len(lineAfterOthers) == len(allWordsLengths) == len(wordsEndProbs)), "%d==%d==%d" % (
            len(lineAfterOthers), len(allWordsLengths), len(wordsEndProbs))
        layout = go.Layout(
            title='Char probablility for %s' % datasetName,
            hovermode="closest",
            legend=go.Legend(y=0.5),
            margin=go.Margin(
                l=0,
                r=0,
                b=0,
                t=30,
            ),
            showlegend=True,
            scene=go.Scene(
                aspectmode="cube",
                xaxis=go.XAxis(title='chars from most probable',
                               range=[0, maxXaxis],
                               tick0=0,
                               ),
                yaxis=go.YAxis(title='probability',
                               ticksuffix='%',
                               tick0=0,
                               dtick=20,
                               range=[0, 100],
                               ),
                zaxis=go.ZAxis(title='position in word',
                               tick0=0,
                               dtick=10,
                               range=[0, len(charCount) + 1]
                               ),
                camera=dict(
                    center=dict(x=0, y=0, z=0),
                    eye=dict(x=0.971, y=-0.050, z=-1.736),
                    up=dict(x=-0.031, y=0.998, z=-0.046),
                ),
                dragmode="orbit"
            )
        )
        data.insert(0, go.Scatter3d(
            x=lineAfterOthers,  # chars
            y=wordsEndProbs,
            z=allWordsLengths,
            marker=go.Marker(
                size=1,
            ),
            line=go.Line(
                width=1
            ),
            name="% of words ends at position",
            hoverinfo="text",
            text=["%.2f%% of words ends at %d position" % (perc, pos) for perc, pos in
                  zip(wordsEndProbs, allWordsLengths)]
        ))
        data.insert(1, go.Scatter3d(
            x=[None],
            y=[None],
            z=[None],
            name="<position>:<chars from most probable>",
            # set opacity = 0
            line={'color': 'rgba(0, 0, 0, 0)'}
        ))

        fig = go.Figure(data=data, layout=layout)
        plotDiv = plotly.offline.plot(fig, validate=False,
                                      output_type="div",
                                      include_plotlyjs=False,
                                      show_link=False)
        divId = PlotUtils.getPlotDivId(plotDiv)
        htmlContent = htmlTemplate.format(plotContentDiv=plotDiv, resizeScript=resizeScript.format(id=divId))
        fileName = "%s/%s_id=%s.html" % (STAT_PLOTS_OUTPUT_DIR, datasetName, wordsStats[DATASET_ID])
        with open(fileName, 'w') as f:
            f.write(htmlContent)
        print "generated: %s\n " % fileName

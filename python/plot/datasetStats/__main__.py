import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../../")

from ProbabilityPlot import ProbabilityPlot
from plot.Consts import *
from plot.PlotUtils import PlotUtils


def plotFromFile(filename):
    wordsStats = PlotUtils.paresJsonFile(BENCH_RESULTS_DIR + "/" + filename)
    ProbabilityPlot.makePlot(wordsStats, filename.replace(".json", ""))


def main():
    print "This tool for every dataset makes plot of probabilities of letter occurrence " \
          "at given position in word to visualize how coefficient of skewness impacts character distribution\n" \
          + WHERE_TO_RUN_INFO
    sys.stdout.flush()
    PlotUtils.assure_path_exists(STAT_PLOTS_OUTPUT_DIR + "/")
    for file in os.listdir(BENCH_RESULTS_DIR):
        if file.endswith(".json"):
            plotFromFile(file)
            print file + " was read\n"
    print "success - end\n"


if __name__ == "__main__":
    main()

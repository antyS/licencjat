#ifndef LICENCJAT_TESTUTILS_H
#define LICENCJAT_TESTUTILS_H

#include <string>
#include <vector>

using  namespace std;
class TestUtils {

public:
    static void check(bool expr, const string &message = "assertion failed");

    static void assertPercentageFromZeroToHundred(std::vector<double> percentages);
};


#endif //LICENCJAT_TESTUTILS_H

#include <iostream>
#include "TestUtils.h"
#include <stdexcept>
#include <vector>


void TestUtils::check(bool expr, const string &message) {
    if (!expr) {
        throw runtime_error(message);
    }
}

void TestUtils::assertPercentageFromZeroToHundred(vector<double> percentages) {
    for (double p:percentages) {
        TestUtils::check(p <= 1.0, "percentage cannot be above 100%");
        TestUtils::check(p > 0.0, "percentage must be above 0.0%");
    }
}
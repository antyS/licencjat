


#include <vector>
#include <string>

using namespace std;

vector<string> readOneWordPerLine(string filePath, int &charsCount);

vector<string> readAndPrintWordsStats(string filePath);

double skewnessFactorOfFile(string filePath);


#include <regex>
#include <fstream>

#include "../../src/generator/WordGenerator.h"
#include "../common/libs/catch.hpp"
#include "../../src/analyser/WordAnalyzer.h"//noinspection
//#include "../../src/analyser/WordAnalyzer.h"//noinspection
#include "WordAnalyzerTest.h"//noinspection
//#include "WordAnalyzerTest.h"//noinspection


TEST_CASE("Skewness by Trie thesame as skewness by Loop", "[trievsloop]") {
    for (int wc:{100, 999, 3099}) {
        for (double skew:{0.01, 0.5, 0.78}) {
            for (int avgWLen:{5, 10, 15}) {
                WordGenerator wg(avgWLen, ALPHABET_SIZE, skew);
                vector<string> words = wg.generateSkewed(wc);
                REQUIRE_MESSAGE(
                        WordAnalyzer::skewnessFactorByTrie(words) == WordAnalyzer::skewnessFactorByLoop(words),
                        "two methots should have the same outcomes");
            }
        }
    }
}

TEST_CASE("Computed vs listed skewness for various dataset params", "[risingskewness]") {
    for (int wc:{100, 999, 3099}) {
        cout << "wordCount=" << wc << endl;
        for (int avgWLen:{5, 10, 15}) {
            cout << "avgWordLen=" << avgWLen << endl;
            vector<double> skewnesses;
            cout << "listedSkewness: ";
            for (double skew:{0.01, 0.05, 0.5, 0.78, 0.82}) {
                cout << skew << " ";
                WordGenerator wg(avgWLen, ALPHABET_SIZE, skew);
                vector<string> words = wg.generateSkewed(wc);
                skewnesses.push_back(WordAnalyzer::skewnessFactorByTrie(words));
            }
            cout << endl << "computedSkewness: " << skewnesses[0] << " ";
            for (int i = 1; i < skewnesses.size(); i++) {
                cout << skewnesses[i] << " ";
                CHECK_MESSAGE(skewnesses[i - 1] < skewnesses[i], "Computed skewness should be rising");
            }
            cout << endl;
        }
    }
}

TEST_CASE("Skewness should be 10.95 or  11.75 or 12.94", "[skewnessinpaper]") {
    for (string filePath:{"datasets/circ2.10K", "datasets/circ2.100K"}) {
        auto factor = skewnessFactorOfFile(filePath);
        CHECK((factor == 10.95 || factor == 11.75 || factor == 12.94));
    }
}

double skewnessFactorOfFile(string filePath) {
    vector<string> words = readAndPrintWordsStats(filePath);
    auto factorByTrie = WordAnalyzer::skewnessFactorByTrie(words);
    std::cout << "\nskewnessFactorByTrie(words):" << factorByTrie
              << std::endl;
    return factorByTrie;
}


vector<string> readOneWordPerLine(string filePath, int &charsCount) {
    charsCount = 0;
    std::ifstream file;
    file.open(filePath);
    vector<string> words;
    string word;
    while (file.good()) {
        file >> word;
        word += '\n';
        words.push_back(word);
        charsCount += word.size();
    }
    file.close();
    return words;
}

vector<string> readAndPrintWordsStats(string filePath) {
    int charsCount;
    vector<string> words = readOneWordPerLine(filePath, charsCount);
    std::cout << "\nfilePath:" << filePath << "\navgWordLen:" << (double) charsCount / words.size()
              << "\nwords.size():" << words.size();
    return words;
}



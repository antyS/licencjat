#include <libs/catch.hpp>
#include "../../src/generator/WordGenerator.h"



TEST_CASE("constructed alphabet properties") {
    CHECK(WordGenerator::constructAlphabet(26) == "abcdefghijklmnopqrstuvwxyz");
    string alphabet = WordGenerator::constructAlphabet(255);
    CHECK(alphabet.size() == ALPHABET_SIZE);
    uint8_t minumum = 255, maximum = 0;
    for (char s:alphabet) {
        INFO(to_string((unsigned char) s));
        CHECK(s != 0);
        CHECK(!std::isspace(s));
        minumum = min(minumum, (uint8_t) s);
        maximum = max(maximum, (uint8_t) s);
    }
    CHECK(minumum == 33);
    CHECK(maximum == MAX_CHAR);
}
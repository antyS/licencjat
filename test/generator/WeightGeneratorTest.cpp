#include <sstream>
#include "libs/catch.hpp"
#include "../../src/generator/ProbabilityGenerator.h"
#include "../../src/generator/WordGenerator.h"
#include "../../src/Consts.h"

using Catch::Matchers::WithinULP;

void testOthers(double others, CharProbabilityDescriptor charProb);

void testFirstWeight(double lastWeightBegin, const CharProbabilityDescriptor &descriptor);

void testWeightGenerator(unsigned int alphabetSize, unsigned int wordLen);

SCENARIO("weightGenerator", "[weightGenerator]") {
    GIVEN("inputs") {
        testWeightGenerator(ALPHABET_SIZE, 500);
        testWeightGenerator(26, 500);
        testWeightGenerator(26, 50);
        testWeightGenerator(26, 5);
    }
}

void testOthers(double others, CharProbabilityDescriptor charProb) {
    double rest = 1.0;
    auto fProb = charProb.distribution.probabilities();
    for (int i = 1; i < fProb.size(); ++i) {
        rest -= fProb[i - 1];
        INFO(" i=" + to_string(i))
        CHECK(fProb[i] == Approx(rest * others).margin(0.1));
    }
}

void testFirstWeight(double lastWeightBegin, const CharProbabilityDescriptor &descriptor) {
    double firstProbability = descriptor.distribution.probabilities()[0];
    CHECK(firstProbability == Approx(lastWeightBegin).margin(0.1));
}

void testWeightGenerator(unsigned int alphabetSize, unsigned int wordLen) {
    stringstream ss;
    ss << " alphabetSize=" << alphabetSize << " avgWordLen=" << wordLen;
    string suffix = ss.str();
    INFO(suffix);
    string alphabet = WordGenerator::constructAlphabet(alphabetSize);
    double firstWeightBegin = 0.9;
    double firstOthers = 0.6;
    double lastWeightBegin = 0.3;
    double lastOthers = 0.1;
    WHEN(string("generated") + suffix) {
        auto probs = ProbabilityGenerator::generateProbabilities(wordLen, alphabet, firstWeightBegin, firstOthers,
                                                                 lastWeightBegin, lastOthers);
        THEN("word len" + suffix) {
            CHECK(probs.size() == wordLen);
        }
        THEN("alfabets shuffled" + suffix) {
            string lastAlphabet = alphabet;
            for (auto p:probs) {
                CHECK(p.sortedAlphabet != alphabet);
                CHECK(p.sortedAlphabet != lastAlphabet);
                lastAlphabet = p.sortedAlphabet;
            }
        }
        THEN("firstWeight equals first probability of start letter" + suffix) {
            testFirstWeight(firstWeightBegin, probs[0]);
        }
        THEN("lastWeight Begin equals first probability of end letter" + suffix) {
            testFirstWeight(lastWeightBegin, probs.back());
        }
        THEN("form second to last probabilities are rest*firstOthers" + suffix) {
            testOthers(firstOthers, probs[0]);
        }
        THEN("form second to last probabilities are rest*lastOthers" + suffix) {
            testOthers(lastOthers, probs.back());
        }
        THEN("all descriptors have sizes equal alphabet.size()" + suffix) {
            for (const auto &p:probs) {
                CHECK(p.sortedAlphabet.size() == alphabet.size());
                CHECK(p.distribution.probabilities().size() == alphabet.size());
            }
        }
    }
}

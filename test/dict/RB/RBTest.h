#ifndef LICENCJAT_RBTEST_H
#define LICENCJAT_RBTEST_H


#include <libs/catch.hpp>
#include "../DicTestBase.h"

class RBTest : public DicTestBase {
public:
    void checkGoodTree(Node *root) override;

    uint32_t checkRedBlack(RBNode *node);

    DicTestBase *newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) override;

    RBTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest);
};


#endif //LICENCJAT_RBTEST_H

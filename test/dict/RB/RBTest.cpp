
#include "../../../src/common/Utils.h"
#include "../../../src/dict/Bst/BstNode.h"
#include "RBTest.h"

#define DICT_NAMES {RB}

TEST_CASE("RBTest small") {
    DicTestBase::testSmall(new RBTest(0, {}), DICT_NAMES);
}

TEST_CASE("RBTest big", "[!hide]") {
    DicTestBase::testBig(new RBTest(0, {}), DICT_NAMES);
}

void RBTest::checkGoodTree(Node *root) {
    if (root != NULL) {
        REQUIRE_MESSAGE(!((RBNode *) root)->isRed, "The root must be black");
        checkRedBlack((RBNode *) root);
    }
}

RBTest::RBTest(uint32_t treeSize, const vector<string> &dictsNamesToTest) : DicTestBase(treeSize,
                                                                                        dictsNamesToTest) {}

uint32_t RBTest::checkRedBlack(RBNode *node) {
    if (node != nullptr) {
        uint32_t leftBlackHeight = checkRedBlack((RBNode *) node->left);
        uint32_t rightBlackHeight = checkRedBlack((RBNode *) node->right);
        REQUIRE_MESSAGE(leftBlackHeight == rightBlackHeight,
                        "\nEvery path from a given node to any of its descendant NIL nodes must contain the same number of black nodes\n");
        if (node->isRed) {
            REQUIRE_MESSAGE((node->left == nullptr || !((RBNode *) node->left)->isRed),
                            "\nLeft node of red is red!\n");
            REQUIRE_MESSAGE((node->right == nullptr || !((RBNode *) node->right)->isRed),
                            "\nRight node of red is red!\n");
            return rightBlackHeight;
        } else {
            return rightBlackHeight + 1;
        }
    } else {
        return 0;
    }
}

DicTestBase *RBTest::newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) {
    return new RBTest(treeSize, dictsNamesToTest);
}


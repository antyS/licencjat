#ifndef LICENCJAT_SPLAYTEST_H
#define LICENCJAT_SPLAYTEST_H


#include <libs/catch.hpp>
#include "../DicTestBase.h"
#include "../Bst/BstTest.h"

class SplayTest : public BstTest {
public:
    SplayTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest);

    DicTestBase *newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) override;

private:
    void checkSplay(string &key, uint32_t val, Dict &tree, bool isOperationSuccess) override;
};


#endif //LICENCJAT_SPLAYTEST_H

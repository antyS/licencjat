
#include "../../../src/common/Utils.h"
#include "../../../src/dict/Bst/BstNode.h"
#include "SplayTest.h"
#define DICT_NAMES {SPLAY}

TEST_CASE("SplayTest small") {
    DicTestBase::testSmall(new SplayTest(0,{}),DICT_NAMES);
}

TEST_CASE("SplayTest big", "[!hide]") {
    DicTestBase::testBig(new SplayTest(0,{}),DICT_NAMES);
}

SplayTest::SplayTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest) : BstTest(
        treeSize,
        dictsNamesToTest) {}

void SplayTest::checkSplay(string &key, uint32_t val, Dict &tree, bool isOperationSuccess) {
    if (isOperationSuccess) {
        REQUIRE_MESSAGE(((BstNode *) tree.root)->key == key, "key= " + key + " val= " + to_string(val));
        if (val != NULL)
            REQUIRE_MESSAGE(((BstNode *) tree.root)->val == val, "key= " + key + " val= " + to_string(val));
    }
}

DicTestBase *SplayTest::newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) {
    return new SplayTest(treeSize, dictsNamesToTest);
}



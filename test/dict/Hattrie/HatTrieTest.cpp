
#include "../../../src/common/Utils.h"
#include "HatTrieTest.h"
#define DICT_NAMES {HAT_TRIE}

TEST_CASE("HatTrieTest small") {
    DicTestBase::testSmall(new HatTrieTest(0, {}), DICT_NAMES);
}

TEST_CASE("HatTrieTest big", "[!hide]") {
    DicTestBase::testBig(new HatTrieTest(0, {}), DICT_NAMES);
}

HatTrieTest::HatTrieTest(uint32_t treeSize, const vector<string> &dictsNamesToTest)
        : DicTestBase(
        treeSize,
        dictsNamesToTest) {}


void HatTrieTest::checkGoodTree(Node *root) {
}

DicTestBase *HatTrieTest::newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) {
    return new HatTrieTest(treeSize, dictsNamesToTest);
}






#ifndef LICENCJAT_HATTRIETEST_H
#define LICENCJAT_HATTRIETEST_H


#include <libs/catch.hpp>
#include "../DicTestBase.h"
#include "../Bst/BstTest.h"

class HatTrieTest : public DicTestBase {
public:
    HatTrieTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest);

    DicTestBase *newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) override;

    void checkGoodTree(Node *root) override;

};


#endif //LICENCJAT_HATTRIETEST_H

#include "libs/catch.hpp"
#include "../../../../src/dict/Hattrie/HashMap/HashMap.h"

using namespace std;

TEST_CASE("HashMap encode decode int to string", "[encDec]") {
    for (unsigned int i = 0; i<= 10; ++i) {
        uint32_t val = 429496729*i;
        string key = "aa";
        WARN("encode and decode key = " + key + " val=" + to_string(val)+" i="+ to_string(i));

        auto encoded = HashMap::encodeInString(key, val);
        auto decodedKeySize = HashMap::decodeSizeFromString(encoded, 0);
        auto decodedValue = HashMap::decodeValueFromString(encoded, 2 + key.size());
        CHECK(decodedKeySize == key.size());
        CHECK(decodedValue == val);
        CHECK(key == encoded.substr(2, key.size()));
    }
}

SCENARIO("HashMap burst", "[burst]") {
    HashMap hashMap = HashMap(nullptr);
    GIVEN("HashMap with one long item") {
        uint32_t value = 312342;
        auto key = "Test";
        hashMap.insert(key, value);
        WHEN("burst") {
            auto burst = hashMap.burst();
            auto newHashMap = (HashMap *) burst->nodes[key[0]];
            THEN("contains value in hash map") {
                REQUIRE(newHashMap->search("est") == value);
                REQUIRE(newHashMap->size == 1);
            }
        }

    }
    GIVEN("HashMap with one char item") {
        uint32_t value = 1231999999;
        auto key = "T";
        hashMap.insert(key, value);
        WHEN("burst") {
            auto burst = hashMap.burst();
            auto newHashMap = (HashMap *) burst->nodes[key[0]];
            THEN("contains value on node") {
                REQUIRE(newHashMap->val == value);
                REQUIRE(newHashMap->size == 1);
            }
        }
    }
    GIVEN("HashMap val on node") {
        REQUIRE(hashMap.size == 0);
        uint32_t value = 1231999999;
        hashMap.val = value;
        hashMap.size++;
        WHEN("burst") {
            auto burst = hashMap.burst();
            THEN("preserves value on  new node") {
                REQUIRE(burst->val == value);
                REQUIRE(burst->size == 1);
            }
        }
    }
}

#include "../../../src/common/Utils.h"
#include "CompactTrieTest.h"
#define DICT_NAMES {TRIE_COMPACT}

TEST_CASE("CompactTrieTest small") {
    DicTestBase::testSmall(new CompactTrieTest(0, {}), DICT_NAMES);
}

TEST_CASE("CompactTrieTest big", "[!hide]") {
    DicTestBase::testBig(new CompactTrieTest(0, {}), DICT_NAMES);
}


CompactTrieTest::CompactTrieTest(uint32_t treeSize, const vector<string> &dictsNamesToTest)
        : DicTestBase(
        treeSize,
        dictsNamesToTest) {}


void CompactTrieTest::checkGoodTree(Node *root) {
    checkCompactTrie((CompactTrieNode *) root);
}


void CompactTrieTest::checkCompactTrie(CompactTrieNode *node) {
    if (node != nullptr) {
        if (node->childCount == 0 && node->parent != nullptr) {
            REQUIRE_MESSAGE(node->val != NULL, "Trie leaf must have value");
        } else {
            uint32_t childCount = 0;
            for (auto const &c :node->children) {
                if (c != nullptr) {
                    childCount++;
                    checkCompactTrie(c);
                }
            }
            REQUIRE_MESSAGE(childCount == node->childCount, "child count has invalid val");
            if (node->parent != nullptr && node->val == NULL) {
                REQUIRE_MESSAGE(node->childCount >= 2, "non leaf without val must have at least 2 children");
            }
        }
    }
}

DicTestBase *CompactTrieTest::newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) {
    return new CompactTrieTest(treeSize, dictsNamesToTest);
}




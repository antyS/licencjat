#ifndef LICENCJAT_COMPACTTRIETEST_H
#define LICENCJAT_COMPACTTRIETEST_H


#include <libs/catch.hpp>
#include "../DicTestBase.h"
#include "../Bst/BstTest.h"

class CompactTrieTest : public DicTestBase {
public:
    CompactTrieTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest);

    void checkGoodTree(Node *root) override;

    DicTestBase *newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) override;

    void checkCompactTrie(CompactTrieNode *node);
};


#endif //LICENCJAT_COMPACTTRIETEST_H

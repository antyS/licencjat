#include <utility>

#include "Bst/BstTest.h"
#include "../../src/dict/Hattrie/HashMap/HashMap.h"
#include "../../src/measurment/MeasureHelper.h"
#include "../../src/common/Utils.h"//noinspection
//#include "../../src/common/Utils.h"
#include <random>

#pragma clang diagnostic push
#pragma ide diagnostic ignored "IncompatibleTypes"

using namespace std;


void DicTestBase::run() {
    WARN(" treeSize:" << treeSize);
    vector<pair<string, Dict *>>
            dicts = Utils::generateDicts(dictsNamesToTest);
    for (auto dict:dicts) {
        WARN("testDict(" + dict.first + ")");
        WARN(MeasureHelper::measureTimeAndMemory([&]() -> void { testDict(*dict.second); })
                     << "\n");
        WARN("testDictSorted(" + dict.first + ")");
        WARN(MeasureHelper::measureTimeAndMemory([&]() -> void { testDictSorted(*dict.second); })
                     << "\n");
        WARN("testDictKeysSequential0ToTreeSize(" + dict.first + ")");
        WARN(MeasureHelper::measureTimeAndMemory([&]() -> void {
            testDictKeysSequential0ToTreeSize(*dict.second);
        })
                     << "\n");

        delete dict.second;
    }
}

void DicTestBase::testDictKeysSequential0ToTreeSize(Dict &dict) {
    string firstKeyBefore = dataSet[0].first;
    auto tmp = dataSet;
    for (int i = 0; i < dataSet.size(); ++i) {
                dataSet[i].first = to_string(i);
            }
    CHECK(dataSet[0].first != firstKeyBefore);
    testDict(dict);
    dataSet = tmp;
    CHECK(dataSet[0].first == firstKeyBefore);
}

void DicTestBase::testDict(Dict &tree) {
    srand(0);//more deterministic rand
    stlMap.clear();
    int i = 0;
    WARN(" insertionTime:"
                 << MeasureHelper::measureTimeAndMemory([&]() -> void {
                     for (auto &pair:dataSet) {
                         insert(pair.first, pair.second, tree);
                     }
                 })
                 << "\n"
    );
    checkMapContentThesameAs(tree, STAGE);
    INFO("\n_______________Start mixed operations______________________\n");
    WARN("mixed operations time: "
                 << MeasureHelper::measureTimeAndMemory([&]() -> void {
                     testMixed(tree);
                 })
                 << "\n mixed operations: " << treeSize << "\n"
    );
    checkMapContentThesameAs(tree, STAGE);
    INFO("\n_______________Start removing______________________\n");
    WARN("removing time: "
                 << MeasureHelper::measureTimeAndMemory([&]() -> void {
                     for (auto &pair:dataSet) {
                         removeTree(pair.first, tree);
                     }
                 })
                 << "\n"
    );
    checkMapContentThesameAs(tree, STAGE);
    WARN("removeLeftover time: "
                 << MeasureHelper::measureTimeAndMemory([&]() -> void {
                     removeLeftover(tree);
                 })
                 << "\n"
    );
    checkMapContentThesameAs(tree, STAGE);
    INFO("\ntest passed\n");
}


void DicTestBase::testMixed(Dict &tree) {
    for (auto &pair:dataSet) {
        uint32_t mod = rand() % 3;
        if (mod == 0) {
            insert(pair.first, pair.second, tree);
        } else if (mod == 1) {
            removeTree(pair.first, tree);
        } else if (mod == 2) {
            search(pair.first, tree);
        } else {
            throw runtime_error("illegal state");
        }
    }
}

void DicTestBase::removeLeftover(Dict &tree) {
    vector<pair<string, int> > keysLeft;
    for (const auto &item:stlMap) {
        keysLeft.emplace_back(item);
    }
    shuffle(keysLeft.begin(), keysLeft.end(), std::mt19937(11));
    for (const auto &s:keysLeft) {
        INFO(s.first << ":" << s.second);
        removeTree(s.first, tree);
    }
}

void DicTestBase::testDictSorted(Dict &tree) {
    srand(0);//more deterministic rand
    vector<pair<string, unsigned int>> sorted = dataSet;

    sort(sorted.begin(), sorted.end());
    INFO("\n_______________InsertingSortedASC______________________\n");
    for (int i = 0; i < sorted.size(); i++) {
        insert(sorted[i].first, sorted[i].second, tree);
    }
    checkMapContentThesameAs(tree, STAGE);
    INFO("\n_______________DeletingSortedDESC______________________\n");
    for (int i = sorted.size() - 1; i >= 0; i--) {
        removeTree(sorted[i].first, tree);
    }
    checkMapContentThesameAs(tree, STAGE);
    INFO("\n_______________InsertingSortedDESC______________________\n");
    for (int i = sorted.size() - 1; i >= 0; i--) {
        insert(sorted[i].first, sorted[i].second, tree);
    }
    checkMapContentThesameAs(tree, STAGE);
    INFO("\n_______________DeletingSortedDESC______________________\n");
    for (int i = sorted.size() - 1; i >= 0; i--) {
        removeTree(sorted[i].first, tree);
    }
    checkMapContentThesameAs(tree, STAGE);
    INFO("\ntest Insert sorted ASC, del sorted DESC passed\n");
}


bool DicTestBase::removeTree(string key, Dict &tree) {
//    INFO( "D k=" << key << "\n");
    uint32_t sizeBefore = tree.size;
    REQUIRE_MESSAGE(sizeBefore == stlMap.size(), "key= " + key);
    bool mapErased = stlMap.erase(key) == 1;
    bool result = tree.remove(key);
    uint32_t sizeAfter = tree.size;
    REQUIRE_MESSAGE(mapErased == result, "key= " + key);
    if (mapErased) {
        REQUIRE_MESSAGE(sizeAfter == sizeBefore - 1, "key= " + key);
    } else {
        REQUIRE_MESSAGE(sizeAfter == sizeBefore, "key= " + key);
    }
    REQUIRE_MESSAGE(sizeAfter == stlMap.size(), "key= " + key);
    REQUIRE_MESSAGE(tree.search(key) == NULL, "key= " + key);
    checkGoodTree(tree.root);
    checkMapContentThesameAs(tree, OPERATION);
    return result;
}

uint32_t DicTestBase::search(string key, Dict &tree) {
    uint32_t result = tree.search(key);
    auto iterator = stlMap.find(key);
    bool mapPresent = iterator != stlMap.end();
    bool treePresent = result != NULL;
    REQUIRE_MESSAGE(treePresent == mapPresent, "key= " + key);
    if (mapPresent) {
        REQUIRE_MESSAGE(result == iterator->second, "key= " + key);
    }
    checkSplay(key, result, tree, treePresent);
    return result;
}

bool DicTestBase::insert(string key, uint32_t val, Dict &tree) {
//    INFO( "I k=" << key << " val=" << val << "\n");
    REQUIRE_MESSAGE(tree.size == stlMap.size(), "key= " + key + " val= " + to_string(val));

    bool isTreeInserted = tree.insert(key, val);
    bool isMapInserted = stlMap.insert(pair<string, int>(key, val)).second;

    REQUIRE_MESSAGE(isTreeInserted == isMapInserted, "key= " + key + " val= " + to_string(val));
    REQUIRE_MESSAGE(tree.size == stlMap.size(), "key= " + key + " val= " + to_string(val));
    REQUIRE_MESSAGE(search(key, tree) == stlMap.find(key)->second, "key= " + key + " val= " + to_string(val));
    checkGoodTree(tree.root);
    checkSplay(key, val, tree, isTreeInserted);
    checkMapContentThesameAs(tree, OPERATION);
    return isTreeInserted;

}


void DicTestBase::checkMapContentThesameAs(Dict &tree, string &mode) {
    if (any_of(whenCheckContentThesame.begin(), whenCheckContentThesame.end(), [&](string s) { return s == mode; })) {
        for (auto item:stlMap) {
            REQUIRE_MESSAGE(tree.search(item.first) == item.second, " keyThatShouldBePresentInTree= " + item.first);
        }
    }

}

DicTestBase::DicTestBase(uint32_t treeSize, vector<string> dictsNamesToTest) :
        treeSize(treeSize),
        dictsNamesToTest(std::move(dictsNamesToTest)) {
    dataSet.reserve(treeSize);
    for (int i = 0; i < treeSize; i++) {
        dataSet.emplace_back(genKey(), Utils::genVal());
    }
}

string DicTestBase::genKey() {
    auto result = to_string(rand() % (treeSize * 10));
    return result;
}

bool DicTestBase::isStringDigit(string num) {
    return (num.find_first_not_of("0123456789") == string::npos);
}

void DicTestBase::addWhenCheckContentThesame(const vector<string> &array) {
    DicTestBase::whenCheckContentThesame.insert(
            this->whenCheckContentThesame.begin(), array.begin(), array.end());
}

void DicTestBase::testSmall(DicTestBase *tClass, const vector<string> &dictsNames) {
    for (int i = 1; i <= 3; i++) {
        auto test = tClass->newInstance(pow(10, i), dictsNames);
        test->addWhenCheckContentThesame({OPERATION});
        test->run();
        delete test;
    }
    delete tClass;
}

void DicTestBase::testBig(DicTestBase *tClass, const vector<string> &dictsNames) {
    for (int i = 4; i <= 6; i++) {
        auto test = tClass->newInstance(pow(10, i), dictsNames);
        test->addWhenCheckContentThesame({STAGE});
        test->run();
        delete test;
    }
    delete tClass;
}

DicTestBase::~DicTestBase() {

}


#pragma clang diagnostic pop

#include "../../../src/common/Utils.h"
#include "../../../src/dict/Bst/BstNode.h"
#include "AvlTest.h"

#define DICT_NAMES {AVL}

TEST_CASE("AvlTest small") {
    DicTestBase::testSmall(new AvlTest(0, {}), DICT_NAMES);
}

TEST_CASE("AvlTest big", "[!hide]") {
    DicTestBase::testBig(new AvlTest(0, {}), DICT_NAMES);
}

void AvlTest::checkGoodTree(Node *root) {
    isAVL((AvlNode *) root);

}

AvlTest::AvlTest(uint32_t treeSize, const vector<string> &dictsNamesToTest) : DicTestBase(treeSize,
                                                                                          dictsNamesToTest) {}

uint32_t AvlTest::isAVL(AvlNode *node) {
    if (node == nullptr) {
        return 0;
    }
    uint32_t rightHeight = isAVL((AvlNode *) node->right);
    uint32_t leftHeight = isAVL((AvlNode *) node->left);
    int factor = leftHeight - rightHeight;
    REQUIRE(abs(factor) < 2);
    REQUIRE(factor == node->balanceFactor);
    return max(rightHeight, leftHeight) + 1;
}

DicTestBase *AvlTest::newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) {
    return new AvlTest(treeSize, dictsNamesToTest);
}

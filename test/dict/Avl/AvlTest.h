#ifndef LICENCJAT_AVLTEST_H
#define LICENCJAT_AVLTEST_H


#include <libs/catch.hpp>
#include "../DicTestBase.h"

class AvlTest : public DicTestBase {
public:
    void checkGoodTree(Node *root) override;

    uint32_t isAVL(AvlNode *node);

    DicTestBase * newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) override;

    AvlTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest);
};


#endif //LICENCJAT_AVLTEST_H

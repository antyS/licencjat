
#include "../../../src/common/Utils.h"
#include "../../../src/dict/Bst/BstNode.h"
#include "BstTest.h"

#define DICT_NAMES {BST}

TEST_CASE("BstTest small") {
    DicTestBase::testSmall(new BstTest(0, {}), DICT_NAMES);
}

TEST_CASE("BstTest big", "[!hide]") {
    DicTestBase::testBig(new BstTest(0, {}), DICT_NAMES);
}

void BstTest::checkGoodTree(Node *root) {
    checkIsBST((BstNode *) root);

}

void BstTest::checkIsBST(BstNode *node) {
    std::stack<BstNode *> s;
    s.push(node);
    while (!s.empty()) {
        node = s.top();
        s.pop();
        if (node != nullptr) {
            REQUIRE(isStringDigit(node->key));
            REQUIRE((node->right == nullptr || node->right->key > node->key));
            REQUIRE((node->left == nullptr || node->left->key < node->key));
            s.push(node->left);
            s.push(node->right);
        }
    }
}

BstTest::BstTest(uint32_t treeSize, const vector<string> &dictsNamesToTest) : DicTestBase(treeSize,
                                                                                          dictsNamesToTest) {}

DicTestBase *BstTest::newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) {
    return new BstTest(treeSize, dictsNamesToTest);
}


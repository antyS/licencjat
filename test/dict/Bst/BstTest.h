#ifndef LICENCJAT_BSTTEST_H
#define LICENCJAT_BSTTEST_H


#include <libs/catch.hpp>
#include "../DicTestBase.h"

class BstTest : public DicTestBase {
public:
    void checkGoodTree(Node *root) override;

    void checkIsBST(BstNode *node);

    DicTestBase *newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) override;

    BstTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest);
};


#endif //LICENCJAT_BSTTEST_H

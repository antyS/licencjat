#ifndef LICENCJAT_TRIETEST_H
#define LICENCJAT_TRIETEST_H


#include <libs/catch.hpp>
#include "../DicTestBase.h"
#include "../Bst/BstTest.h"

class TrieTest : public DicTestBase {
public:
    TrieTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest);

    DicTestBase *newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) override;

    void checkGoodTree(Node *root) override;

    void checkTrie(TrieNodeBase *node);

    uint32_t checkEveryChild(TrieNodeBase *node);
};


#endif //LICENCJAT_TRIETEST_H

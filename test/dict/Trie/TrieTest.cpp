
#include "../../../src/common/Utils.h"
#include "TrieTest.h"

#define DICT_NAMES {TRIE_LIST, TRIE_ARRAY}

TEST_CASE("TrieTest small") {
    DicTestBase::testSmall(new TrieTest(0,{}),DICT_NAMES);
}

TEST_CASE("TrieTest big", "[!hide]") {
    DicTestBase::testBig(new TrieTest(0,{}),DICT_NAMES);
}

TrieTest::TrieTest(uint32_t treeSize,  const vector<string> &dictsNamesToTest) : DicTestBase(
        treeSize,
        dictsNamesToTest) {}

void TrieTest::checkTrie(TrieNodeBase *node) {
    if (node != nullptr) {
        if (node->childCount == 0 && node->parent != nullptr) {
            REQUIRE_MESSAGE(node->val != NULL, "Trie leaf must have value");
        } else {
            uint32_t childCount = checkEveryChild(node);
            REQUIRE_MESSAGE(childCount == node->childCount, "child count has invalid val");
        }
    }
}

void TrieTest::checkGoodTree(Node *root) {
    checkTrie((TrieNodeBase *) root);
    REQUIRE_MESSAGE(((TrieNodeBase *) root)->val == NULL, "Trie root cannot have value");

}

uint32_t TrieTest::checkEveryChild(TrieNodeBase *node) {
    uint32_t childCount = 0;
    if (dynamic_cast<TrieListNode *>(node)) {
        for (auto const &c :((TrieListNode *) node)->children) {
            childCount++;
            checkTrie(c.second);
        }
    }
    if (dynamic_cast<TrieArrayNode *>(node)) {
        for (auto const &c :((TrieArrayNode *) node)->children) {
            if (c != nullptr) {
                childCount++;
                checkTrie(c);
            }
        }
    }
    return childCount;
}

DicTestBase *TrieTest::newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) {
    return new TrieTest(treeSize, dictsNamesToTest);
}




#ifndef LICENCJAT_DICTTEST_H
#define LICENCJAT_DICTTEST_H

#include <regex>
#include <iostream>
#include <map>
#include "../../src/dict/Dict.h"
#include "../../src/dict/Trie/array/TrieArrayNode.h"
#include "../../src/dict/Avl/AvlNode.h"
#include "../../src/dict/RB/RBNode.h"
#include "../../src/dict/Trie/list/TrieListNode.h"
#include "../../src/dict/CompactTrie/CompactTrieNode.h"
#include <cmath>

static string STAGE = "checkAfterStage";
static string OPERATION = "checkAfterOperation";

using std::string;
using namespace std;

class DicTestBase {

    uint32_t treeSize;
    vector<pair<string, unsigned int>> dataSet;
    vector<string> dictsNamesToTest;
    std::map<string, int> stlMap;
    vector<string> whenCheckContentThesame = {};
public:
    DicTestBase(uint32_t treeSize, vector<string> dictsNamesToTest);

    virtual ~DicTestBase();

    void run();

    void addWhenCheckContentThesame(const vector<string> &array);

    static bool isStringDigit(string num);

    virtual DicTestBase *newInstance(uint32_t treeSize, vector<string> dictsNamesToTest) = 0;

    static void testSmall(DicTestBase *test, const vector<string> &dictsNames);

    static void testBig(DicTestBase *test, const vector<string> &dictsNames);

private:

    void testDict(Dict &tree);

    void testDictSorted(Dict &tree);

    void testMixed(Dict &tree);

    void removeLeftover(Dict &tree);

    virtual void checkGoodTree(Node *root) = 0;

    virtual void checkSplay(string &key, uint32_t val, Dict &tree, bool isOperationSuccess) {};

    uint32_t search(string key, Dict &tree);

    bool insert(string key, uint32_t val, Dict &tree);

    bool removeTree(string key, Dict &tree);


    void checkMapContentThesameAs(Dict &tree, string &mode);

    string genKey();

    void testDictKeysSequential0ToTreeSize(Dict &dict);
};

#endif //LICENCJAT_DICTTEST_H
